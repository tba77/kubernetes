#!/bin/bash
echo "Installing metal-LB"
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.14.8/config/manifests/metallb-native.yaml
sleep 1

docker network inspect -f '{{.IPAM.Config}}' kind

kubectl apply -f https://kind.sigs.k8s.io/examples/loadbalancer/metallb-config.yaml

echo "Install Ingress controller"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
