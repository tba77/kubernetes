### Question 1
##### Question Content:
Create a new deployment called nginx-deploy, with image nginx:1.16 and 1 replica. Next upgrade the deployment to version 1.17 using rolling update.

Make sure that the version upgrade is recorded in the resource annotation. Next sclae the deployment to 3 replicas.

<details>
<summary><b>Question Answer:</b></summary>

```bash
kubectl create deploy nginx-deploy --image=nginx:1.16
kubectl set image deployments/nginx-deploy nginx=nginx:1.17 --record
```

To check if the version is recorded or not we can run:

```bash
kubectl rollout history deployment/nginx-deploy
kubectl scale deploy nginx-deploy --replicas=3
```

</details>

### Question 2
##### Question Content:

Create a secret called secret-cka with the following Parameters:

- User:secret-volume
- pwd:cka123

Create a pod called secret-1401 in the admin1401 namespace using the busybox image.

The container within the pod should be called secret-admin and should sleep for 4800 seconds.

The container should mount a read-only secret volume called secret-volume at the path /etc/secret-volume.

<details>
<summary><b>Question Answer</b></summary>

```bash
kubectl create ns admin1401

kubectl create secret generic secret-cka --from-literal=user=secret-volume --from-literal=pwd=cka123 --namespace=admin1401

Kubectl run secret-1401 --namespace=admin1401 --image=busybox --command sleep 4800 --dry-run=client -o yaml > secret-1401.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: secret-1401
  name: secret-1401
  namespace: admin1401
spec:
  containers:
  - command:
    - sleep
    - "4800"
    image: busybox
    name: secret-admin                  #we need to change it
    volumeMounts:                       #add
    - name: secret-volume               #add
      mountPath: "/etc/secret-volume"   #add
      readOnly: true                    #add
  volumes:                              #add
  - name: secret-volume                 #add
    secret:                             #add
      secretName: secret-cka            #add    
```

let’s check if our pod in running state or not, to do that we run:

```bash
kubectl get pods -n admin1401
```

</details>

### Question 3
##### Question Content:
Take the backup of ETCD at the location /tmp/etcd-backup.db on the master node.

<details>
<summary><b>Question Answer:</b></summary>

We can use the k8s documentation: https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/

So do to a backup we need some infos of the etcd pod yaml definition

Exam Tipps to save some mins:

Cat /etc/kubernetes/manifests/etcd.yaml
And take the following infos:

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    component: etcd
    tier: control-plane
  name: etcd
  namespace: kube-system
spec:
  containers:
  - command:
    - etcd
    - --advertise-client-urls=https://192.168.103.11:2379
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt  #use                            
    - --client-cert-auth=true
    - --data-dir=/var/lib/etcd
    - --initial-advertise-peer-urls=https://192.168.103.11:2380
    - --initial-cluster=cluster3-master1=https://192.168.103.11:2380
    - --key-file=/etc/kubernetes/pki/etcd/server.key   #use                           
    - --listen-client-urls=https://127.0.0.1:2379,https://192.168.103.11:2379   # use
    - --listen-metrics-urls=http://127.0.0.1:2381
    - --listen-peer-urls=https://192.168.103.11:2380
    - --name=cluster3-master1
    - --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt
    - --peer-client-cert-auth=true
    - --peer-key-file=/etc/kubernetes/pki/etcd/peer.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt  #use                  
    - --snapshot-count=10000
    - --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    image: k8s.gcr.io/etcd:3.3.15-0
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /health
        port: 2381
        scheme: HTTP
      initialDelaySeconds: 15
      timeoutSeconds: 15
    name: etcd
    resources: {}
    volumeMounts:
    - mountPath: /var/lib/etcd
      name: etcd-data
    - mountPath: /etc/kubernetes/pki/etcd
      name: etcd-certs
  hostNetwork: true
  priorityClassName: system-cluster-critical
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/etcd
      type: DirectoryOrCreate
    name: etcd-certs
  - hostPath:
      path: /var/lib/etcd  #important in case we want to restore db
      type: DirectoryOrCreate
    name: etcd-data
status: {}
```

```bash
sudo ETCDCTL_API=3 etcdctl --cacert /etc/kubernetes/pki/etcd/ca.crt  \
--cert /etc/kubernetes/pki/etcd/server.crt \ 
--key /etc/kubernetes/pki/etcd/server.key \
snapshot save /tmp/etcd-backup.db  \
```

Check snapshot with:

```bash
ETCDCTL_API=3 etcdctl --write-out=table snapshot status /tmp/etcd-backup.db -w table
```

</details>

### Question 4

##### Question Content:

A kubeconfig file called cka.kubeconfig has been created under /home/admin.

There is something wrong with the configuration, troubleshoot and fix it.

<details>

<summary><b> Question Answer: </b></summary>

So first let’s try to run a command to check what type of errors we get:

```bash
kubectl cluster-info –kubeconfig=/home/admin/cka.kubeconfig
```

We can see that the connection to the server was refused: the reason is that the etcd port number is wrong

```bash
vi /home/admin/cka.kubeconfig
```
```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ………
    server: https://172.17.0.47:2379   #change port to 6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: ………
    client-key-data: ……… 
```

</details>

### Question 5

##### Question Content:

Print the names of all Pods with there Namespace, NodeName, HostIP, Phase and the startTime and sort it by the creationTimestamp in the following format:

POD_NAME NAMESPACE NODE_NAME HOST_IP PHASE START_TIME

<pod name> <namespace> <node_name> <host_ip><phase><start_time>

Example

etcd-controlplane kube-system controlplane 172.17.0.37 Running 2020-12-09T19:42:52Z

Write the result to the file /home/admin2406_data.txt

<details>
<summary>Question Answer:</summary>

```bash
kubectl get po -A -o=custom-columns=POD_NAME:.metadata.name,NAMESPACE:.metadata.namespace,NODE_NAME:.spec.nodeName,HOST_IP:.status.hostIP,PHASE:.status.phase,START_TIME:.metadata.creationTimestamp --sort-by=.metadata.creationTimestamp > /home/admin2406_data.txt
```

</details>

### Question 6

#### Question Content:

Create an nginx pod called nginx-resolver using image nginx.

Expose it internally with a service called nginx-resolver-service.

Test that you are able to look up the service name from within the cluster.

Use the image: busybox:1.28 for dns lookup.

Record results in /home/CKA/nginx.svc

<details>
<summary><b>Question Answer:</b></summary>

Let’s create our nginx pod:

```bash
kubectl run nginx-resolver --image=nginx --restart=Never
```

Once done, let’s expose the nginx pod as a ClusterIP service

```bash
kubectl expose pod nginx-resolver --name=nginx-resolver-service --port=80
```

Now we will create a test pod to test if we can make a dnslookup

```bash
kubectl run test-pod --image=busybox:1.28 --restart=Never --command sleep 3600<code>
kubectl exec -it test-pod -- nslookup nginx-resolver-service.default.svc.cluster.local > /home/CKA/nginx.svc
```

</details>

### Question 7

##### Question Content:

Use JSON PATH query to retrieve the osImages of all the nodes.

Store it in a file /home/nodes_os.txt

<details>

<summary><b>Question Answer:</b></summary>

```bash
kubectl get nodes -o jsonpath='{.items[*].status.nodeInfo.osImage}' > /home/nodes_os.txt
```

</details>

### Question 8

##### Question Content:

Create a new user called john and grant him access to the cluster via CertificateSigningRequest and approve this Request.

John should have permission to create, list, get, update and delete pods in the development namespace.

Create the required role and role bindings for the user John

The Role name should be: dev-role

The RoleBinding name should be: dev-role-binding

The private key are stored under: /home/CKA/john.key and csr at /home/CKA/john.csr

<details>
<summary><b>Question Answer:</b></summary>

We will use the documentations to create a CSR request

https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/#create-a-certificate-signing- request-object-to-send-to-the-kubernetes-api

Let’s create a file and named johnreq.yaml

```yaml
vi johnreq.yaml
```

Now let’s copie the yaml from the k8s docuemtations:

```yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: my-svc.my-namespace	#change the name
spec:
  request: $(cat server.csr | base64 | tr -d '
\n')	#change that
  signerName: kubernetes.io/kubelet-serving
  usages:
  - digital signature
  - key encipherment
  - server auth                
```

Now let’s encode our john.csr file on base64. For that we will use the command already provided in the yaml in k8s documentations

```bash
cat /home/CKA/john.csr | base64 | tr -d '\n'
```

Let’s copie our output terminal and paste it in our yaml under request:


```yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: john
spec:
  groups:
  - system:authenticated
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZEQ0NBVHdDQVFBd0R6RU5NQXNHQTFVRUF3d0VhbTlvYmpDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRApnZ0VQQURDQ0FRb0NnZ0VCQUtSWFlrVWJIRS9aSjRQTTVJN1JzbmpBcEVhbDBpK2lzQVdlT1JoL3c2R0ZKdENTCkRwQi9NSzVKcHFhWkpjclNXWS9xNmtoMmtabXEzWkU2bXNJbDN3VjlYMUZFRWkwU0FDdzJKMm0rUURpV1ZtTXkKaEdLdFBKbjIvWHRRRE1ETXZEcFBHeWp6djVWOVI4em5oRlhveHNta0dlU0sxeWVuTlVHRTVvcERiWU1kWFhGdgpGWmwyR1RHTE1td2pWbS9RQjNIUThxVG5XQ2g2ekNJdmh5MEZsdUtRR244aUJrekdUYnRWY0drc2tvWVg3ZlhzCjFwOHh4a0NZTWlMWjVvbDRTM2VUUHU5QU5JK0tRZldMUUZZSGRPcnUyTSs3WHROWHh3N2p1VlVSRUlkSjF4dHMKdFRGdHNHaDd4d2RUa3pQTzVCU2tlR0VwUFRFeitqbVVTQ2k4bHYwQ0F3RUFBYUFBTUEwR0NTcUdTSWIzRFFFQgpDd1VBQTRJQkFRQnhIRWx3bnVtM0RvMklXMmp6eSttRUNyRWhnRFRqTjJ4Mzk0QlJZaXBKeWlWdVJpVG03ZEowCm1KdEhDZ2ZDZXYyTHJ2MzFNcW1ickU3djNqT1JOcHFFOFlqamFvZGFxbE05Vm93ZUNERjAwUUZtZElwWk45b2MKZk5IK3ZZMEhoL040RjYxSCt1UVdFSVBSNVhPenhrWHpWcHovT2VYeUx5cjR6MWgwNENJa29iOWhQcENqWlNyTQpNUFlYanI1eU1GN1VTc0FxMUJDYlM1bldCOWtCUWw4OW1zL3BwMWdzUG5tTnNFTzFWU3B1WWtwd0pYSFhHUkd3CmRrRS9xYlM2dmV2eFZHTVNJNEtoQ1BuWGZvMlRRQjJXa05ybk96aGZib1k2ckdpZjFsQU1xa2UzR3J1Z3kyTUMKRXpSS21yOGxyZzFRUlJCd2pBUzhCM3YycjBEQis3d2sKLS0tLS1FTkQgQ0VSVElGSUNBVEUgUkVRVUVTVC0tLS0tCg==
signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
```

Now let’s check

```bash
kubectl get csr
```

Now we need to approve this Signing Request:

```bash
kubectl certificate approve john
```

once approved, we will start by creating the Role and namespace

Now let’s create our namespace:

```bash
kubectl create ns development 
kubectl create role dev-role  --verb=get,list,create,update,delete --resource=pods --namespace=development
```

Nice, Now our Role-Binding for the User John:

```bash
kubectl create rolebinding dev-role-binding --role=dev-role --user=john --namespace=development
```

Now we can check it by running:

```bash
kubectl auth can-i get pods --namespace=development --as=john
yes
```

```bash
kubectl auth can-i get svc --namespace=development --as=john
no
```

</details>

### Question 9

##### Question Content:

You have access to multiple clusters from your main terminal through kubectl contexts. Write all those context names into /home/contexts.txt

Next write a command to display the current context into /home/context_default_kubectl.sh, the command should use kubectl.

Finally write a second command doing the same thing into /home/context_default_no_kubectl.sh, but without the use of kubectl

<details>

<summary><b>Question Answer:</b></summary>


kubectl config get-contexts or using jsonpath:

```bash
 kubectl config view -o jsonpath="{.contexts[*].name}"
 ```

Or

```bash
kubectl config view -o jsonpath="{.contexts[*].name}" | tr " " "\n" > /home/contexts.txt #for_new_line
```

Next create the first command:

```bash
# /home/context_default_kubectl.sh
```

```bash
echo "kubectl config current-context" > /home/context_default_kubectl.sh
```

And the second one:
```bash
# /home/context_default_no_kubectl.sh

echo "cat ~/.kube/config | grep current" > /home/context_default_no_kubectl.sh<text>
```

In the real exam you might need to filter and find information from bigger lists of resources, hence knowing a little jsonpath and simple bash filtering will be helpful.

</details>

### Question 10

##### Question Content:

There are various pods in all namespaces.

Write a command into /home/find_pods.sh which lists all pods sorted by their AGE.

Write a second command into /home/find_pods_uid.sh which lists all pods sorted by field metadata.uid.

Use kubectl sorting for both commands.

<details>
<summary><b>Question Answer:</b></summary>

A great resource for this is the kubectl cheat sheet. You can quickly reach it if you search for "cheat sheets" in the Kubernetes documents.

```bash
Echo "kubectl get pod --all-namespaces --sort-by=.status.startTime“ > /home/find_pods.sh
Echo "kubectl get pod --all-namespaces --sort-by=.metadata.uid“ > /home/find_pods_uid.sh
```

</details>

### Question 11

##### Question Content:

Create a second kube-scheduler named my-scheduler which can just be a copy from the existing default one.

Make sure both schedulers are running along side each other correctly.

Create a pod named use-my-cka-scheduler image httpd:2.4-alpine which uses the new scheduler and confirm the pod is running.

<details>

<summary><b>Question Answer:</b></summary>

```bash
cd /etc/kubernetes/manifests/
sudo cp kube-scheduler.yaml my-cka-scheduler.yaml
sudo vim my-cka-scheduler.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    component: kube-scheduler
    tier: control-plane
  name: my-cka-scheduler                               # change
  namespace: kube-system
spec:
  containers:
  - command:
    - kube-scheduler
    - --authentication-kubeconfig=/etc/kubernetes/scheduler.conf
    - --authorization-kubeconfig=/etc/kubernetes/scheduler.conf
    - --bind-address=127.0.0.1
    - --kubeconfig=/etc/kubernetes/scheduler.conf
    - --leader-elect=false                              # change
    - --scheduler-name=my-cka-scheduler                 # add
    - --port=12345                                      # change
    - --secure-port=12346                               # add
    image: k8s.gcr.io/kube-scheduler:v1.19.1
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /healthz
        port: 12346                                     # change
        scheme: HTTPS
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    name: kube-scheduler
    resources:
      requests:
        cpu: 100m
    startupProbe:
      failureThreshold: 24
      httpGet:
        host: 127.0.0.1
        path: /healthz
        port: 12346                                     # change
        scheme: HTTPS
```

We need to set the `--leader-elect=false` here, because we don't want to select a leader between the two existing schedulers but having both function at the same time. The leader election is used when multiple master nodes are setup and with that multiple schedulers, then there should always only be one active, that's the leader.

We need also to specify different ports for the new one, otherwise our new scheduler will crash and give you this error in the scheduler pod logs:

`failed to create listener: failed to listen on 0.0.0.0:10251: listen tcp 0.0.0.0:10251: bind: address already in use`

Once done, save and check status, give it a bit of time or move the yaml files out of the manifests dir and back into again.

```bash
kubectl -n kube-system get pod
```

Create a test pod for the new scheduler:

```bash
kubectl run use-my-cka-scheduler --image=httpd:2.4-alpine --dry-run=client -o yaml > use-my- scheduler.yaml
```

```bash
vi use-my-scheduler.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: use-my-cka-scheduler
  name: use-my-cka-scheduler
spec:
  schedulerName: my-cka-scheduler     # add
  containers:
  - image: httpd:2.4-alpine
    name: use-my-cka-scheduler
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Create it and check its status:

```yaml
kubectl get pod -o wide
```

</details>

### Question 12

##### Question Content:

Use namespace project-ds for the following:

Create a daemonset named ds-important with image httpd:2.4-alpine and labels id=ds-important.

The pods it creates should request 100 millicore cpu and 10 megabytes memory.

The pods of that daemonset should run on all nodes.

<details>
<summary><b> Question Answer:</b></summary>

We aren't able to create a daemonset directly using kubectl, so we create a deployment and just change it up:

```bash
 kubectl create ns project-ds
 kubectl -n project-ds create deployment --image=httpd:2.4-alpine ds-important --dry-run=client -o yaml > ds.yaml
 vi ds.yaml 
```

```yaml
apiVersion: apps/v1
kind: DaemonSet 			#change it
metadata:
  creationTimestamp: null
  labels:
    id: ds-important
  name: ds-important
  namespace: project-ds
spec:
  #replicas: 1			#delete it
  selector:
    matchLabels:
      id: ds-important
  #strategy: {}			#delete it
  template:
    metadata:
      creationTimestamp: null
      labels:
        id: ds-important
    spec:
      containers:
      - image: httpd:2.4-alpine
        name: httpd
        resources:
          requests:
            cpu: 100m
            memory: 10Mi
      tolerations:
      - effect: NoSchedule
        key: node-role.kubernetes.io/master                
```
The trick in the question is "The pods must run in all nodes" so it includes controlplane, for that we must add a to the pod to be scheduled on controlplane also here

```yaml
tolerations:
- effect: NoSchedule
  key: node-role.kubernetes.io/master  
```

```bash
kubectl create -f ds.yaml
```
</details>
### Question 13

##### Question Content:

Create a pod named multi-container in namespace default with three containers, named c1, c2 and c3. There should be a volume attached to that pod and mounted into every container, but the volume shouldn't be persisted or shared with other pods.

Container c1 should be of image nginx:1.17.6-alpine and have the name of the node where its pod is running on value available as environment variable MY_NODE_NAME.

Container c2 should be of image busybox:1.31.1 and write the output of the date command every second in the shared volume into file date.log. You can use while true; do date >> /your/vol/path/date.log; sleep 1; done for this.

Container c3 should be of image busybox:1.31.1 and constantly write the content of file date.log from the shared volume to stdout. You can use tail -f /your/vol/path/date.log for this.

Check the logs of container c3 to confirm correct setup.

<details>

<summary><b> Question Answer: </b></summary>

First we create the pod template:

```bash
kubectl run multi-container --image=nginx:1.17.6-alpine --dry-run=client -o yaml > multi- container.yaml 
```

```bash
vi multi-container.yaml
```

```yaml
 apiVersion: v1
 kind: Pod
 metadata:
   labels:
     run: multi-container
   name: multi-container
 spec:
   containers:
   - image: nginx:1.17.6-alpine
     name: c1                                                                    
     resources: {}
     env:
     - name: MY_NODE_NAME                                                        
       valueFrom:                                                           
         fieldRef:     
           fieldPath: spec.nodeName                                              
     volumeMounts:
     - name: vol                                                                 
       mountPath: /vol                                                           
   - image: busybox:1.31.1                                                       
     name: c2                                                                    
     command: ["sh", "-c", "while true; do date >> /vol/date.log; sleep 1; done"]
     volumeMounts:
     - name: vol                                                                 
       mountPath: /vol                                                           
   - image: busybox:1.31.1                                                       
     name: c3                                                                    
     command: ["sh", "-c", "tail -f /vol/date.log"]                              
     volumeMounts:     
     - name: vol                                                                 
       mountPath: /vol                                                           
   dnsPolicy: ClusterFirst
   restartPolicy: Always
   volumes:
     - name: vol                                                                 
       emptyDir: {}                                                              
 status: {}
```

```bash
kubectl create -f multi-container.yaml
```

We check if everything is good with the pod:

```bah
kubectl get po
```

We check if container c1 has the requested node name as env variable:

```bash
kubectl execmulti-container -c c1 -- env | grep MY
```

Finally we check the logging:

```bash
kubectl logs multi-container -c c3
```

</details>

### Question 14
##### Question Content:

There seems to be an issue with the kubelet not running on Worker-node node1.

Fix it and confirm that the cluster has node1 available in Ready state afterwards. Schedule a pod called task14 with a nginx image on node1.

Make sure that the pod in Running State.

<details>

<summary><b> Question Answer: </b></summary>

So it seems that our Kubelet is not running on Node1. We can test this by creating a pod and we can see that the status of this pod is in pending state.

So let’s ssh into the node and check:

```bash
ssh node1

sudo systemctl status kubelet
```

we can see that the kubelet is stoped and it’s not in running state, so let’s try to start it.

```bash
sudo systemctl start kubelet
```

Nice, it’s seems to be that, all good now and our kubelet is in Running state. So let’s schedule a pod on this node:

```bash
kubectl run task14 --image=nginx -o yaml --dry-run=client > task14.yaml

vi task14.yaml                
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: task14
  name: task14
spec:
  nodeName: node1
  containers:
  - image: nginx
    name: task14
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
</details>

### Question 15

##### Question Content:

Install kubernetes on the following cluster (you are currently on the master instance) and make sure that the node1 (ssh node1) are in ready state. Docker is already pre-installed.

You have only one Master and one Worker node.

<details>

<summary><b>Question Answer: </b></summary>

We will make use of the k8s documentation: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

A best practice is first to check the os version: (Our Os is Ubuntu)

```bash
cat /etc/os-release
```

So let’s check the installation steps for Ubuntu, we will start by updating the package and installing the keys for k8s:


```bash
 sudo apt-get update && sudo apt-get install -y apt-transport-https curl

 curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

 cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
 deb https://apt.kubernetes.io/ kubernetes-xenial main 
 EOF 
 ```

Once done, we will do an update and install the kubelet, kubeadm and kubectl for k8s:


```bash
 sudo apt-get update

 sudo apt-get install -y kubelet kubeadm kubectl

 sudo apt-mark hold kubelet kubeadm kubectl
 ```

We can check by running this command:

```bash
kubeadm version
```

Now let’s repeate the same steps on node01

```bash
ssh node01
```

```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https curl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

Once done, let’s chek the version of kubectl:

```bash
Kubectl version
```

Now let’s bootstrap our k8s cluster, for that we need to ssh back to the master Node (https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

```bash
Kubeadm init
```

That’s can take up to 5min, to start using your cluster, you need to run the following as a regular user:

```bash
mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

You can now join any number of machines by running the following on each node as root:

```bash
sudo kubeadm join <control-plane-host>:<control-plane-port> --token <token> --discovery-token-ca-cert-hash sha256:<hash>
```

You should now deploy a Pod network to the cluster, in our case we will use weave:

```bash
exit
```

```bash
kubectl apply -f https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')
```

</details>

### Question 16

##### Question Content:

Create a 'Persistent Volume' with the given specification:
- Volume Name: pv-log
- Storage: 100Mi
- Access modes: ReadWriteMany
- Host Path: /workdir/logs

Once done, create a PersistentVolumeClaim called claim-log-1 with the given specification:
- Volume Name: claim-log-1
- Storage Request: 50Mi

Create a Pod named pod-claim, image nginx and mount the PVC on it.

<details>

<summary><b> Question Answer: </b></summary>

For the PersistentVolume yaml we will copie it from the k8s documentation: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes

```bash
vi pv-log.yaml
```

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-log
spec:
  capacity:
    storage: 100Mi
  hostPath:
    path: /workdir/logs
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
```

```bash
 Kubectl create -f pv-log.yaml
 Kubectl get pv 
 ```

Now, we will create the PersistentVolumeClaim. For that we will use again the k8s docu: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims

```bash
vi claim-log-1.yaml
```

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: claim-log-1
spec:
  accessModes:
    - ReadWriteMany
  volumeMode: Filesystem
  resources:
    requests:
      storage: 50Mi
```

Once our PVC is created, let’s check it, for that we can run the both commands and see if the satus of the pv and the pvc is `bound` or not

```bash
kubectl get pv 
```

```bash
kubectl get pvc
```

Now once our pv and pvc are created and ready, let’s create our Pod and mount the pvc on it:

```bash
kubectl run pod-claim --image=nginx --restart=Never --dry-run=client -o yaml > pod-claim.yaml 

vi pod-claim.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-claim
spec:
  containers:
  - image: nginx
    name: pod-claim
    volumeMounts:
    - mountPath: /workdir/logs
      name: pv-log
  volumes:
  - name: pv-log
    persistentVolumeClaim:
      claimName: claim-log-1                
```

Once our yaml file is ready we can create our pod:

```bash
Kubectl create -f pod-claim.yaml
```

We need to check if our pod is in running state or not, for that we run:

```bash
Kubectl get pods
```

 </details>