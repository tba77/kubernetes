### STEP 1: integration phase

1. Write docker file for the app DONE

2. Prepare the environment for K8S (minikube + Ingress controller ) DONE

3. Write the right files for app deployment.

   1. use simple service / deployment.yml ( do not use a provider terraform / ansible /terragrunt ) DONE

4. optimize your app:

   1. config map DONE
   2. secrets DONE
   3. volumes
   4. replace deployment for db by stateful set

5. monitoring of the app

   1. prometheus + Grafana
   2. log collector (fluentd)

### STEP 2: PRODUCTION PHASE

1. use a cloud provider
2. automate the deployment on cloud provider with terragrunt / form
3. Include CI/CD gitlab / github

### STEP 3: Troubleshooting

### Kubernetes configuration

Kubernetes config file has 3 parts :

1. Metadata: One of the metadata is the name of the component we create
2. Specifications: In where we put every kind of configuration we want to apply
3. Status: Generated automatically by k8s

#### Kubernetes installation steps :

1. Install a container runtime on every node (Masters + Workers)
2. Install Kubelet
   - Both Kubelet and Container Runtime will run as a regular linux process

3. Kube Proxy Application as a pod on every node also
4. On masters : Deploy pods which run master applications
   - API server
   - Scheduler
   - ETCD
   - Controller manager

All pods are installed as static pods and managed directly by kubelet daemon

| Regular pod scheduling  | Static pod scheduling  |
| ----------------------- | ---------------------- |
| API Server gets request | kubelet: schedules pod |
| Scheduler : which node  |                        |
| kubelet : schedules pod |                        |

To schedule pods kubelet watches a specific location `/etc/kubernetes/manifests`

Difference between regular pod and static pod

- Pod names are suffixed with the node hostname
- Kubelet (not controller) watches and restart static pod if it fails

Bootstrap k8s cluster (command to be executed on first master node)

```bash
kubeadm init
```
Connect to cluster

`kubectl get nodes` will give an error message because kubectl doesn’t know where is the cluster we can manage multiple cluster with the same command so we have to pass a config file to give the cluster location

```bash
kubectl get nodes –kubeconfig /etc/kubernetes/admin.conf
```

This can only be done from master node to use another terminal or machine we have to copy admin.conf in `$HOME/.kube/config` or create an access config file for specific user (RBAC)

##### Namespaces :

Create namespace

```bash
kubectl create namespace my-name-space
```
list namespaced resources

```bash
kubectl api-resources --namespaed=true
```
list non namespaced resources

```bash
kubectl api-resources --namespaced=false
```
configMap and secrets can’t be shared across namespaces however we can access services within another namespace the url would be `service-name.namespace`

list pods running in a specific namesape

```bash
kubectl get pods -n namespace-name
```
```bash
kubectl get pods -n kube-system
```

**Networking**:

- each pod have its own IP address
- Pods solve allocating ports problem
- When a pod is created on a node it gets its own network namespace and vrtual ethernet connection
- A pod is a host with its ip address and a range of ports to allocate to its containers
- you may have a maximum of 6 containers in one pod
- inside a same pod containers communicate with each other via localhost and a port number
- Per each pod there is a pause container called sandbox containers whose only job is to reserve and hold pod network namespace
- pause container enable pods to communicate with each other
- pause container ensure when a container dies that the new one has the same IP address than the old one

**Container Networking Interface (CNI)**

kubernetes defines rules for networking but doen’t implement a networking interfaces the rules are:

- Each pod with unique IP address
- Each pod can communicate with other in the same node
- Each pod can communicate with another pod in other nodes without a NAT

How CNI implements these rules:

- Each node has an IP address belonging to the same LAN
- On each node a LAN is created with different IP range by the network plugin and the IP address musn’t overlap node’s IP range
- To ensure that every pod gets a unique IP across nodes, the network plugin set an IP range for the whole cluster and give equal subsets to each node.
- To communicate through each nodes gateways are used for communication between pods

**Join nodes to the cluster**

To join a cluster we need the join command with some parameters and token to identify nodes which has been printed on STDOUT when initializing the master we can generate again the command if we lost what have been printed

```bash
kubeadm token create --print-join-command
```
Cheking weavnet:

In its deployment weavnet has 2 pods inside a container so to check weavenet we must specify the container to check

```bash
kubectl logs pod-name -n kube-system -c container-name
```

To deploy an application quickly for test we can run

```bash
kubectl run pod-name --image=imageName
```

##### Kubernetes components

###### Deployment configuration file

1. Template
   - has its own metadata and specification
   - it’s a configuration file inside a configuration file and the reason is that section is applied to a pod
   - Spec is the blue print of a pod (which image will be used, which port etc...)

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template: # pod configuration part
    metadata:
      labels:
        app: nginx
    spec: # Pod blue print
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

To deploy our application 
```bash
kubectl apply -f path-to-config-file.yaml
```

###### Service

- Servces make pods accessible in a permanent IP address using the service name
- It loadbalances traffic among the pods
- Service is not a process running on the nodes
- It's a virtual IP address accessible throughout the cluster

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

- spec section in service component is a configuration about ports

**How to connect the service to the right pods ?**

Labels and selectors are used for that
  - Labels attach a key-value pair to components which should be meaningful and revelant to users
  - Selectors identify components which have a certain label on them
  
How to check that our serivce is connected to the pod? for that we have a kubectl sub-command that gives details about a component

```bash
kubectl describe component component-name
```
We can also check the endpoints of a service with the command 

```bash
kubectl get ep
```

**Kube-Proxy**

- Kube-proxy forwards requests from services to it's processes which are the pods
- Kube-proxy is responsible for maintaining a list of sevices IPs and corresponding Pods IPs

Check Status

As we know a configuration file has 3 parts 
- metadata
- specification
- status


Status is added by kubernetes to check a component status we have 2 ways 

```bash
kubectl edit component-type component-name
```
```bash
kubectl get component-type component-name -o yaml
```

example

```bash
kubectl edit service nginx-service
```

```bash
kubectl get service nginx-service -o yaml
```

**Labeles**

- Labeling components in one of the best practices of kubernetes and a standard approche, somes labels are optionnal but it's better to label all components.
- Each component in a cluster has a name that is unique for that type of resource.
- We can't have the same name for two resources in the same namespace (2 deployments with the same name or 2 services with the same name).

To check labels we can use describe 

```bash
kubectl describe component component-name`
```
```bash
kubectl describe service myapp-svc
```

```bash
Name:              myapp-svc
Namespace:         default
Labels:            app=myapp
Annotations:       <none>
Selector:          app=myapp
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.96.202.121
IPs:               10.96.202.121
Port:              <unset>  80/TCP
TargetPort:        80/TCP
Endpoints:         10.244.0.11:80
Session Affinity:  None
Events:            <none>
```

Here we have too much information to simply check labels we can use for that 

```bash
kubectl get component --show-labels
```

```bash
kubectl get deployment --show-label
```

```bash
NAME             READY   UP-TO-DATE   AVAILABLE   AGE   LABELS
db-deployment    1/1     1            1           22h   app=db
php-deployment   1/1     1            1           22h   app=myapp
```

it's useful sometimes to select components with their labels rather than their names especially when we have hundreds and we need to select all replicas of a specific pod we can select all pods/replicas related to specific label.

```bash
kubectl get pods -l app=myapp
```

```bash
NAME                             READY   STATUS    RESTARTS   AGE
php-deployment-df468bb85-8djqs   2/2     Running   0          50s
php-deployment-df468bb85-9vkp7   2/2     Running   0          22h
php-deployment-df468bb85-z2n4r   2/2     Running   0          50s
```

This is also useful when we need to check logs for a request for example and we don't know which replica has executed that request

```bash
kubectl logs -l app=myapp --all-containers # --all-container is for when we have sidecar containers for example
```

##### Scaling applications

Sometimes we would like to scale our application for test purposes or something else without modifying our manifest and deploy our application again for that we can use the command.
Scale command can be used for deployment and statefullset 

```bash
kubectl scale --replicas=x component component-name
```

```bash
kubectl scale --replicas=4 deployment php-deployment
```

```bash
$ kubectl get pods
```

```bash
NAME                             READY   STATUS              RESTARTS   AGE
db-deployment-64b4887888-xhwjs   1/1     Running             0          23h
php-deployment-df468bb85-6qxw5   0/2     ContainerCreating   0          2s
php-deployment-df468bb85-97wc7   0/2     ContainerCreating   0          2s
php-deployment-df468bb85-9vkp7   2/2     Running             0          23h
php-deployment-df468bb85-ssfxh   0/2     ContainerCreating   0          2s
```

##### Components history changes

With imperative commands we can't have a history of changes that were made to our cluster and our components when manifests and version control allow us to have an overview about all changes that have been made all team members can view those changes.

However we can add an audit trail of our changes for imperative commands by adding `--record` option to our command. This option can be added to almost kubectl subcommands

```bash
kubectl scale --replicas=4 deployment php-deployment --record`
```

**NB:** --record has been deprecated and will be simply removed form future versions

## DNS in Kubernetes

Kubenetes has a component named CoreDNS that resolves names with their corresponding IP address.
When a service is created in the cluster CoreDNS create a record of that service

When a pod is created a dns entry is added to its `/etc/resolv.conf` file, it's kubelet who is responsible of adding such entry when it schedules a new pod

```bash
kubectl exec -it  php-deployment-df468bb85-9vkp7 -- bash
```

```bash
root@php-deployment-df468bb85-9vkp7:/var/www/html# cat /etc/resolv.conf 
search default.svc.cluster.local svc.cluster.local cluster.local
nameserver 10.96.0.10
options ndots:5
```
10.96.0.10 is CoreDNS service IP and not the CoreDNS Pod IP.

```bash
kubectl get pod -l k8s-app=kube-dns -n kube-system -o wide
```

```bash
NAME                       READY   STATUS    RESTARTS   AGE    IP           NODE                     NOMINATED NODE   READINESS GATES
coredns-558bd4d5db-pwgqs   1/1     Running   4          3d4h   10.244.0.4   training-control-plane   <none>           <none>
coredns-558bd4d5db-zg6f4   1/1     Running   2          3d4h   10.244.0.7   training-control-plane   <none>           <none>
```

```bash
kubectl get svc -l k8s-app=kube-dns -n kube-system -o wide
```

```bash
NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE    SELECTOR
kube-dns   ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   3d4h   k8s-app=kube-dns
```

The DNS server IP address is defined in kubelet configuration

```bash
root@training-control-plane:/# cat /var/lib/kubelet/config.yaml
```

```yaml
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    cacheTTL: 0s
    enabled: true
  x509:
    clientCAFile: /etc/kubernetes/pki/ca.crt
authorization:
  mode: Webhook
  webhook:
    cacheAuthorizedTTL: 0s
    cacheUnauthorizedTTL: 0s
cgroupDriver: cgroupfs
clusterDNS:
- 10.96.0.10
clusterDomain: cluster.local
cpuManagerReconcilePeriod: 0s
evictionHard:
  imagefs.available: 0%
  nodefs.available: 0%
  nodefs.inodesFree: 0%
evictionPressureTransitionPeriod: 0s
fileCheckFrequency: 0s
healthzBindAddress: 127.0.0.1
healthzPort: 10248
httpCheckFrequency: 0s
imageGCHighThresholdPercent: 100
imageMinimumGCAge: 0s
kind: KubeletConfiguration
logging: {}
nodeStatusReportFrequency: 0s
nodeStatusUpdateFrequency: 0s
rotateCertificates: true
runtimeRequestTimeout: 0s
shutdownGracePeriod: 0s
shutdownGracePeriodCriticalPods: 0s
staticPodPath: /etc/kubernetes/manifests
streamingConnectionIdleTimeout: 0s
syncFrequency: 0s
volumeStatsAggPeriod: 0s
```
#####Fully Qualified Domain Name

For each service CoreDNS create a subdomain 

|hostname |Namespace|Type |Root          |
|:---:    |:---:    |:---:|:---:          |
|db-svc    |default  |svc |cluster.local|
|myapp-svc |default  |svc |cluster.local|
|myapp2-svc|myapp-ns |svc |cluster.local|

so the fully qualified domain name for a component is 

`<component-name>.<namespace>.<type>.<root>`

a service should have a FQDN
`<service-name>.<namespace>.svc.cluster.local`

So to access to a service in a different namespace we have to indiquate the namespace subdomain part to be able to access the service.
In the same namespace we can use short names because in /etc/resolv.conf we have the `search` option  which search in all listed subdomains before saying domain not found

for example for default namespace we have

`search default.svc.cluster.local svc.cluster.local cluster.local`

#### Practical tips

**Create kubernetes manifests form declarative commands**

We can create manifests form kubernets imperative commands rather than copy/paste examples from k8s documentation. For that we have just to run a command in dry-run mode and export the output to a yaml file for example

###### create service

```bash
kubectl create service clusterip nginx-svc --tcp=80:80 --dry-run=client -o yaml > nginx-service.yaml
```

###### create deployment

```bash
kubectl create deployment nginx-deployment --image=nginx:latest --port=80 --replicas=2 --dry-run=client -o yaml > nginx-deployment.yaml
```

###### create pod

```bash
kubectl run nginx-pod --image=nginx:latest --port=80 --labels="app=nginx" --dry-run=client -o yaml
```

###Expose services for external access

we have 3 ways to expose a service and make it accessible 
- Node Port
- Load Balancer
- Ingress

##### NodePort

Kubernetes services have a type. Default type is `ClusterIP` which means serivce is accessible by other pods wihtin the cluster.
- NodePort type make it external and for that we need to add two attributes to our manifest `type: NoePort` and `nodePort: <port-number>` this dedicated port is opened on each worker node making the applicaton accessible from external. 
- nodePort range is between 30000 and 32767

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: myapp-svc
  labels:
    app: myapp
spec:
  selector:
    app: myapp
  type: NodePort
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30000
```

##### LoadBalancer

NodePort is not practical 
- it opens a specific port for each accessible service on each node
- we have to keep track of opened ports (which port for which service)
- accessible through ip addresse : port (not easy to remeber)

LoadBalancer comes on top of NodePort and expose a signle entrypoint to services which will be the load balancer IP address or domain name the load balancer then will redirect the traffic to worker nodes on the opened nodePort

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: myapp-svc
  labels:
    app: myapp
spec:
  selector:
    app: myapp
  type: LoadBalancer
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30000
```


Even if we manage to have a single entry point LoadBalancer still has some disadvantages 
- We must have a specific load balancer for each service
- Each load balacner exposes a new port
- How to configure domain name for each application

##### Ingress
Ingress is a part of a kubernetes cluster which get rid of all disadvantages of NodePort and LoadBalancer service type at once
- it have routing routes to reach services
- is the only entry point for all services
- only ingress have to be exposed through node port or a load balancer
- it's deployed and available inside the cluster
- allow to configure secure connection
- ingress component is like a configuration piece for ingress controller
- it's kubernetes native way on configuring routing logic

```yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: php-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: myapp.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: myapp-svc
            port:
              number: 80
```

#### Role Based Access Control (RBAC)

Kubernetes supports 4 authorization modes
- Node
- ABAC (Attribute Based Acces Control)
- RBAC
- Webhook

The autorization mode is configured into api server

```bash 
cat /etc/kubernetes/manifests/kube-apiserver.yaml
```

```yaml
kind: Pod
metadata:
  annotations:
    kubeadm.kubernetes.io/kube-apiserver.advertise-address.endpoint: 172.19.0.4:6443
  creationTimestamp: null
  labels:
    component: kube-apiserver
    tier: control-plane
  name: kube-apiserver
  namespace: kube-system
spec:
  containers:
  - command:
    - kube-apiserver
    - --advertise-address=172.19.0.4
    - --allow-privileged=true
    - --authorization-mode=Node,RBAC
    - --client-ca-file=/etc/kubernetes/pki/ca.crt
    - --enable-admission-plugins=NodeRestriction
    - --enable-bootstrap-token-auth=true
    - --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt
    - --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt
```

##### Roles

- Roles are limited to a namespace
- Define access using roles
- Define namespaced permissions
- Define resources you access to (pod, depoloyments, services....)
- Define what you can do with resources (list, delete, get, update)

Roles define resources and access permissions to those resources it doesn't include informations about hwo gets these access attached to the role

##### Role Binding

- Role binding link a role to a user or group
- Groups or users get access to what is defined into the role

##### ClusterRole & ClusterRole Binding

- ClusterRole defines permissions and resources cluster wide it's not limited to a namespace
- ClusterRole binding attachs cluster wide role and permissions to a user or a group of users which are the admins of the cluster

##### Users & Groups

- Kubernetes doesn't manage natively users and groups
- it relies on external sources to manage users and groups
- No kubernetes object that represent users and groups
- External sources for authentication can be:
      - static token file
      - Certificate
      - 3rd party identity service

##### Serice Account

Besides users we can have applications that need access to the cluster such as monitoring applications or log collectors we should be able to define application roles.

- ServiceAccount defines an application user
- ServiceAccount can be linked to Role with RoleBinding
- ServiceAccount can also be linked to ClusterRole wht ClusterRoleBinding

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: dev-namespace
  name: Developer
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["get", "create", "watch", "list"]
- apiGroups: [""] # "" indicates the core API group
  resources: ["secrets"]
  verbs: ["get"]
```

This role will be applied to the `dev-namespace` namespace, each role has 3 items:
- apiGroups: [""] means Core API (api groups are defined in https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24)
- resources: Pods, dployments, services etc....
- verbs: permissions allowed, get, list (read-only) create, update (read-write)

We can have more granular way to define access for example we can grant permission to only specified pods, for that we use `resourceNames`

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: dev-namespace
  name: Developer
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["get", "create", "watch", "list"]
  resourceNames: ["myapp"]
- apiGroups: [""] # "" indicates the core API group
  resources: ["secrets"]
  verbs: ["get"]
```

To attach roles to user or group of user we have to create a RoleBinding configuration file

```yaml
apiVersion: rbac.authorization.k8s.io/v1
# This role binding allows "jane" to read pods in the "default" namespace.
# You need to already have a Role named "pod-reader" in that namespace.
kind: RoleBinding
metadata:
  name: read-pods
  namespace: Developer
subjects:
# You can specify more than one "subject"
- kind: User
  name: jane # "name" is case sensitive
  apiGroup: rbac.authorization.k8s.io
roleRef:
  # "roleRef" specifies the binding to a Role / ClusterRole
  kind: Role #this must be Role or ClusterRole
  name: pod-reader # this must match the name of the Role or ClusterRole you wish to bind to
  apiGroup: rbac.authorization.k8s.io
  ```
##### Cheking API Access

Kubernetes provides an `auth can-i` subcommand to check access permissions


```bash
kubectl auth can-i create deployments --namespace default
```

#### Client Certificates

On cluster bootstrap kubernetes generates a bunch of certificates that are stored in `/etc/kubernetes/pki` 

```bash
apiserver-etcd-client.crt     apiserver.key       front-proxy-ca.key
apiserver-etcd-client.key     ca.crt              front-proxy-client.crt
apiserver-kubelet-client.crt  ca.key              front-proxy-client.key
apiserver-kubelet-client.key  etcd                sa.key
apiserver.crt                 front-proxy-ca.crt  sa.pub
```
it also generates ca certificates that sign all certificates and copied to all cluster nodes so that all certificates are considered as trusted ones

### Create user on Kubernetes

As we said kubernetes doesn't manage users but users are identified with certificates so to create a user we first have to create a certificate signing request that have to be approved by kubernetes

##### Create keys and CSR

We will use openssl to create the private key and the CSR file 

`openssl genrsa -out user-k8s.key 2048`

- genrsa: we will generate an rsa key
- out : generate the file
- 2048 : length of our private key

To generate the signing request
`openssl req -new -key user-k8s.key -subj "\CN=user-name" -out user-k8s.
csr`

"user-name" is the name that will be used by kubernetes.

##### Validate and approve request

Now that our certificate request is ready we have to submit it to kubernetes API, for that a component have to be created in which we put the content of our certificate signing request in base64 our configuration file looks like

```yaml
---
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: user-name
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZUQ0NBVDBDQVFBd0VERU9NQXdHQTFVRUF3d0ZkR0ZvWVhJd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQQpBNElCRHdBd2dnRUtBb0lCQVFEVlZDUDZKQnpsSGhwUjBZbC9JNlRYVUIvTFZINUdIWGVjcTBVSkpwZ2kvMnUrCmtTNkxROWgvck5yQzM4UzBYQVFuRHZETGxYZllkTTZJRjBLaWZ0MVNDc2Z5blc4R0lOMFI0a0pJZ2xCSkVwS1cKVjI2K2ZPbmp2MzVmbjQvUC9ScGErcHMvOUJnaHF6ZnluVERaZXp4OVdwbkFpVkJuSVRNZHY2L3NtcWlpWnFDTQpVTVdVaUgrNFNkZUNBUExYeFR1RXVxVGpBRG9wM2NwNWJVUGdxUW1vcEY2QUIyWklMR3ZvRjNGaTRReEVBNHp0CnYrV3NWekRmbVlreEhjQks0aldPWk40M2hwbWMzOE1mbVNVK3NVSFh6WUxNUGg3Y1MzTm5SU2JsU3JsQ2hFWjgKNkJ4dEJYVmtkQWtTVjFET0VRa3lCcmNoV2srWWpraWlXNTJQN1VwNUFnTUJBQUdnQURBTkJna3Foa2lHOXcwQgpBUXNGQUFPQ0FRRUFVWUFrWUVQNmgweTdERHdLOTczQlUwQTlGYnFoOTF3dVZQZGRvSkpmdXNpaEJMenpia0FrCkMwWTJwUEFlOWJtRStuMCt0OUovcGZPWGI3ZThWak1OVEUwNE9XaVY0SEk3YTJjSFg2VytiTkFsaWdOaTl3REIKRXhYaHdHWWF5Qm1ndS9mdUZHWHpvdmlmaXEwRVQ0cjVVcFRjbFVtT1U3bHhZQlFoTzNxcHcyc3dnSTYvWlgxYQo3aHhzMnZkczlqUGdLR252Y2tDVGVEcHMrYjNLR094SFE5KzdHcUtBTFY4Y1AwdXloVG96U0s3SGxjT1VwQ1BxCkd6U1o3eWIzclRNaEkrQTgwUGZPUDZMV0trRDNMZnRLdE15KzhYVmN0WXdrTXdMSjlISEMxdmc5ZGVnVjFVL3IKUEtCSktMdHJPT0g2MWJhSTJzaysxT0RSRUxLMHlpazY4QT09Ci0tLS0tRU5EIENFUlRJRklDQVRFIFJFUVVFU1QtLS0tLQo=
  signerName: kubernetes.io/kube-apiserver-client
  # expirationSeconds: 8640000
  usages:
    - client auth
```
```bash
kubectl apply -f user-csr.yaml
```

Then we can see that our certificate is in a pending state 

```bash
kubectl get csr
NAME    AGE        SIGNERNAME                          REQUESTOR          CONDITION
user-name   69m   kubernetes.io/kube-apiserver-client   kubernetes-admin   Pending
```

To approve the signing request we must have an admin access to the cluster and with the command `kubectl certificate` we can approve or deny a request.

```bash
kubect certificate approve user-name
certificatesigningrequest.certificates.k8s.io/tahar approved
```
```bash
kubectl get csr
NAME    AGE   SIGNERNAME                            REQUESTOR          CONDITION
tahar   75m   kubernetes.io/kube-apiserver-client   kubernetes-admin   Approved,Issued
```
We have now to extract our certificate to do that
```bash
kubectl get csr user-name -o yaml
```
```yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"certificates.k8s.io/v1","kind":"CertificateSigningRequest","metadata":{"annotations":{},"name":"user-name"},"spec":{"request":"LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZUQ0NBVDBDQVFBd0VERU9NQXdHQTFVRUF3d0ZkR0ZvWVhJd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQQpBNElCRHdBd2dnRUtBb0lCQVFEVlZDUDZKQnpsSGhwUjBZbC9JNlRYVUIvTFZINUdIWGVjcTBVSkpwZ2kvMnUrCmtTNkxROWgvck5yQzM4UzBYQVFuRHZETGxYZllkTTZJRjBLaWZ0MVNDc2Z5blc4R0lOMFI0a0pJZ2xCSkVwS1cKVjI2K2ZPbmp2MzVmbjQvUC9ScGErcHMvOUJnaHF6ZnluVERaZXp4OVdwbkFpVkJuSVRNZHY2L3NtcWlpWnFDTQpVTVdVaUgrNFNkZUNBUExYeFR1RXVxVGpBRG9wM2NwNWJVUGdxUW1vcEY2QUIyWklMR3ZvRjNGaTRReEVBNHp0CnYrV3NWekRmbVlreEhjQks0aldPWk40M2hwbWMzOE1mbVNVK3NVSFh6WUxNUGg3Y1MzTm5SU2JsU3JsQ2hFWjgKNkJ4dEJYVmtkQWtTVjFET0VRa3lCcmNoV2srWWpraWlXNTJQN1VwNUFnTUJBQUdnQURBTkJna3Foa2lHOXcwQgpBUXNGQUFPQ0FRRUFVWUFrWUVQNmgweTdERHdLOTczQlUwQTlGYnFoOTF3dVZQZGRvSkpmdXNpaEJMenpia0FrCkMwWTJwUEFlOWJtRStuMCt0OUovcGZPWGI3ZThWak1OVEUwNE9XaVY0SEk3YTJjSFg2VytiTkFsaWdOaTl3REIKRXhYaHdHWWF5Qm1ndS9mdUZHWHpvdmlmaXEwRVQ0cjVVcFRjbFVtT1U3bHhZQlFoTzNxcHcyc3dnSTYvWlgxYQo3aHhzMnZkczlqUGdLR252Y2tDVGVEcHMrYjNLR094SFE5KzdHcUtBTFY4Y1AwdXloVG96U0s3SGxjT1VwQ1BxCkd6U1o3eWIzclRNaEkrQTgwUGZPUDZMV0trRDNMZnRLdE15KzhYVmN0WXdrTXdMSjlISEMxdmc5ZGVnVjFVL3IKUEtCSktMdHJPT0g2MWJhSTJzaysxT0RSRUxLMHlpazY4QT09Ci0tLS0tRU5EIENFUlRJRklDQVRFIFJFUVVFU1QtLS0tLQo=","signerName":"kubernetes.io/kube-apiserver-client","usages":["client auth"]}}
  creationTimestamp: "2022-02-21T11:48:32Z"
  name: user-name
  resourceVersion: "15059"
  uid: 4e21a3d5-36bf-4ad1-81c0-606ff3ffc5b4
spec:
  groups:
  - system:masters
  - system:authenticated
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZUQ0NBVDBDQVFBd0VERU9NQXdHQTFVRUF3d0ZkR0ZvWVhJd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQQpBNElCRHdBd2dnRUtBb0lCQVFEVlZDUDZKQnpsSGhwUjBZbC9JNlRYVUIvTFZINUdIWGVjcTBVSkpwZ2kvMnUrCmtTNkxROWgvck5yQzM4UzBYQVFuRHZETGxYZllkTTZJRjBLaWZ0MVNDc2Z5blc4R0lOMFI0a0pJZ2xCSkVwS1cKVjI2K2ZPbmp2MzVmbjQvUC9ScGErcHMvOUJnaHF6ZnluVERaZXp4OVdwbkFpVkJuSVRNZHY2L3NtcWlpWnFDTQpVTVdVaUgrNFNkZUNBUExYeFR1RXVxVGpBRG9wM2NwNWJVUGdxUW1vcEY2QUIyWklMR3ZvRjNGaTRReEVBNHp0CnYrV3NWekRmbVlreEhjQks0aldPWk40M2hwbWMzOE1mbVNVK3NVSFh6WUxNUGg3Y1MzTm5SU2JsU3JsQ2hFWjgKNkJ4dEJYVmtkQWtTVjFET0VRa3lCcmNoV2srWWpraWlXNTJQN1VwNUFnTUJBQUdnQURBTkJna3Foa2lHOXcwQgpBUXNGQUFPQ0FRRUFVWUFrWUVQNmgweTdERHdLOTczQlUwQTlGYnFoOTF3dVZQZGRvSkpmdXNpaEJMenpia0FrCkMwWTJwUEFlOWJtRStuMCt0OUovcGZPWGI3ZThWak1OVEUwNE9XaVY0SEk3YTJjSFg2VytiTkFsaWdOaTl3REIKRXhYaHdHWWF5Qm1ndS9mdUZHWHpvdmlmaXEwRVQ0cjVVcFRjbFVtT1U3bHhZQlFoTzNxcHcyc3dnSTYvWlgxYQo3aHhzMnZkczlqUGdLR252Y2tDVGVEcHMrYjNLR094SFE5KzdHcUtBTFY4Y1AwdXloVG96U0s3SGxjT1VwQ1BxCkd6U1o3eWIzclRNaEkrQTgwUGZPUDZMV0trRDNMZnRLdE15KzhYVmN0WXdrTXdMSjlISEMxdmc5ZGVnVjFVL3IKUEtCSktMdHJPT0g2MWJhSTJzaysxT0RSRUxLMHlpazY4QT09Ci0tLS0tRU5EIENFUlRJRklDQVRFIFJFUVVFU1QtLS0tLQo=
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
  username: kubernetes-admin
status:
  certificate: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM5akNDQWQ2Z0F3SUJBZ0lSQUpjWkVEK3I3dk5KNkVuSTAzeDFkRGN3RFFZSktvWklodmNOQVFFTEJRQXcKRlRFVE1CRUdBMVVFQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TWpBeU1qRXhNalU0TUROYUZ3MHlNekF5TWpFeApNalU0TUROYU1CQXhEakFNQmdOVkJBTVRCWFJoYUdGeU1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBCk1JSUJDZ0tDQVFFQTFWUWoraVFjNVI0YVVkR0pmeU9rMTFBZnkxUitSaDEzbkt0RkNTYVlJdjlydnBFdWkwUFkKZjZ6YXd0L0V0RndFSnc3d3k1VjMySFRPaUJkQ29uN2RVZ3JIOHAxdkJpRGRFZUpDU0lKUVNSS1NsbGR1dm56cAo0NzkrWDUrUHovMGFXdnFiUC9RWUlhczM4cDB3MlhzOGZWcVp3SWxRWnlFekhiK3Y3SnFvb21hZ2pGREZsSWgvCnVFblhnZ0R5MThVN2hMcWs0d0E2S2QzS2VXMUQ0S2tKcUtSZWdBZG1TQ3hyNkJkeFl1RU1SQU9NN2IvbHJGY3cKMzVtSk1SM0FTdUkxam1UZU40YVpuTi9ESDVrbFByRkIxODJDekQ0ZTNFdHpaMFVtNVVxNVFvUkdmT2djYlFWMQpaSFFKRWxkUXpoRUpNZ2EzSVZwUG1JNUlvbHVkaisxS2VRSURBUUFCbzBZd1JEQVRCZ05WSFNVRUREQUtCZ2dyCkJnRUZCUWNEQWpBTUJnTlZIUk1CQWY4RUFqQUFNQjhHQTFVZEl3UVlNQmFBRk14M0x4WitxRHR1UkcvWTlBdlYKMkhvL2VCbjNNQTBHQ1NxR1NJYjNEUUVCQ3dVQUE0SUJBUUJVcE5BZGpxSndlY2tydlI2WWNoZXg0RGFQZzVmRApFa0ZKV1hkcE4xRkxEY1B1ci9ma2tYUFF6TGV6ODRHT2hQZCtlUUNDSVJocWZQS1J6QXpOWHBDVEZiUDVid1VEClV1QVdRWE1nMDZKRGE0QWxVMzJubS9tTWQxY1UxUHNJZUs2VHJKSGwydXJiNEg2K2F4ZS9TUEpnbXg2L3RLVVYKUzZRL2hKYmg1Tm9HWXVKTzFMNzZ6aWpoazZnSlc5L1RsY0RnU3N0UGtDWEU1cGgrbTBrWmV2YXVIbEN0Q1JKZQpjL3YwaDBudzliRWpIcTBTK3phYVZTbkZITnRydXBhMmdmQ2RyY0JvSlpCamEvSjhGMGxwakVBbTNNS0hqRlpSCkovQWRDS0hWUCttbU9xTURMak5ycHpkQ0V0dmF3YmdYOG4rY29iTWVYNjFXUGo4VW9KR2RsRWVTCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
  conditions:
  - lastTransitionTime: "2022-02-21T13:03:02Z"
    lastUpdateTime: "2022-02-21T13:03:02Z"
    message: This CSR was approved by kubectl certificate approve.
    reason: KubectlApprove
    status: "True"
    type: Approved
```
we have to decode the `certificate:` part as it's coded in `base64` 
```bash
echo 'LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM5akNDQWQ2Z0F3SUJBZ0lSQUpjWkVEK3I3dk5KNkVuSTAzeDFkRGN3RFFZSktvWklodmNOQVFFTEJRQXcKRlRFVE1CRUdBMVVFQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TWpBeU1qRXhNalU0TUROYUZ3MHlNekF5TWpFeApNalU0TUROYU1CQXhEakFNQmdOVkJBTVRCWFJoYUdGeU1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBCk1JSUJDZ0tDQVFFQTFWUWoraVFjNVI0YVVkR0pmeU9rMTFBZnkxUitSaDEzbkt0RkNTYVlJdjlydnBFdWkwUFkKZjZ6YXd0L0V0RndFSnc3d3k1VjMySFRPaUJkQ29uN2RVZ3JIOHAxdkJpRGRFZUpDU0lKUVNSS1NsbGR1dm56cAo0NzkrWDUrUHovMGFXdnFiUC9RWUlhczM4cDB3MlhzOGZWcVp3SWxRWnlFekhiK3Y3SnFvb21hZ2pGREZsSWgvCnVFblhnZ0R5MThVN2hMcWs0d0E2S2QzS2VXMUQ0S2tKcUtSZWdBZG1TQ3hyNkJkeFl1RU1SQU9NN2IvbHJGY3cKMzVtSk1SM0FTdUkxam1UZU40YVpuTi9ESDVrbFByRkIxODJDekQ0ZTNFdHpaMFVtNVVxNVFvUkdmT2djYlFWMQpaSFFKRWxkUXpoRUpNZ2EzSVZwUG1JNUlvbHVkaisxS2VRSURBUUFCbzBZd1JEQVRCZ05WSFNVRUREQUtCZ2dyCkJnRUZCUWNEQWpBTUJnTlZIUk1CQWY4RUFqQUFNQjhHQTFVZEl3UVlNQmFBRk14M0x4WitxRHR1UkcvWTlBdlYKMkhvL2VCbjNNQTBHQ1NxR1NJYjNEUUVCQ3dVQUE0SUJBUUJVcE5BZGpxSndlY2tydlI2WWNoZXg0RGFQZzVmRApFa0ZKV1hkcE4xRkxEY1B1ci9ma2tYUFF6TGV6ODRHT2hQZCtlUUNDSVJocWZQS1J6QXpOWHBDVEZiUDVid1VEClV1QVdRWE1nMDZKRGE0QWxVMzJubS9tTWQxY1UxUHNJZUs2VHJKSGwydXJiNEg2K2F4ZS9TUEpnbXg2L3RLVVYKUzZRL2hKYmg1Tm9HWXVKTzFMNzZ6aWpoazZnSlc5L1RsY0RnU3N0UGtDWEU1cGgrbTBrWmV2YXVIbEN0Q1JKZQpjL3YwaDBudzliRWpIcTBTK3phYVZTbkZITnRydXBhMmdmQ2RyY0JvSlpCamEvSjhGMGxwakVBbTNNS0hqRlpSCkovQWRDS0hWUCttbU9xTURMak5ycHpkQ0V0dmF3YmdYOG4rY29iTWVYNjFXUGo4VW9KR2RsRWVTCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K' | base64 --d > user-k8s.crt
```

### Connect to the cluster

Now that we have all our files ready how can we connect to the cluster ?
We have two options here :
- By default kubectl uses `~/.kube/config` file and we can override this file by adding the needed arguments to kubectl command these arguments are 
  - server
  - certificate-authority
  - client-certificate
  - client-key
- Or using a config file located at `~/kube/config` with the right parameters

`kubectl options` gives us all options we can add to `kubect` command

To connect to our cluster we can do
```bash
kubectl --server https://127.0.0.1:43447 \
--certificate-authority ca.cert \
--client-certificate user-k8s.cert \
--client-key user-k8s.key \
get pods
```
Here we will have a permission forbidden as we didn't configure permissions to user `user-name` 

Because passing arguments in each command is actually annoying we will configure a config file that we will pass with `--kubeconfig` argument

```yaml
---
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM1ekNDQWMrZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeU1ESXlNVEV4TURjME0xb1hEVE15TURJeE9URXhNRGMwTTFvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTDhJCitHRHRJMVZTTXplNk91ajF2OFpESmIxRFFidEphbEpYVFFYVDFrcGtoZ0lRamFMNDdyNXNIWHpWdDUzc1QxQkkKVjI1eDFoQjNQZjVGaWhxZ2tBcHF3ZEtJbmZYRE9vbTZ1MUp0cjk1YjFlVk5xNm4ya1dVSTJ5SndKRmVjcjNqMQpsYzVlNm8wMDlkS0NRUkMrZFcxbXBKRTU5MnpPSVo3UUlvaHFtSmpjY2pzOFVrSEFuMm4xSnpOUXdVY2h6QzVxCjY5SlZibklLQXJoV1dNUnlUdjBUS3pGNUNVc0I0TDRHUmRtWkNWYUQzalFGSitlSWhva0o5YTRlTjEzWEFjNmYKZ1dUVGJPRXNhZXRqWGlGRmZDRTdEalhJMlBkZSsrVnVhVWtyS3htMWxoenloNGtEN1R1eEZSKzFHdTdqOUlDawo2NEtrcys3RU1LSGxWc1BPMFlzQ0F3RUFBYU5DTUVBd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZNeDNMeForcUR0dVJHL1k5QXZWMkhvL2VCbjNNQTBHQ1NxR1NJYjMKRFFFQkN3VUFBNElCQVFCRjRVNE9BRG95U2x0TTBocHRrVHZJVEJVVm5yWW5ielN3SVpMRWhZckhJY2hmcmh1YQp0bUpBQXEyaTN0RVRVYWluQTFYK01yNWlrY080NTRxOW1VWkhTbTlGQ1BxU2F5OER5OUs5bzJiZlE4Y29HLzdwCkhDKzY3NWZ3WTdMeVh5bm9ScFczdTF4QlZXejE4UlZyTm1NdkdFTFZRTTZzemJNbStmYTg1R3FkWjRuMy9wZkcKUnNMWnVGQzRnSjR3dUhZcUdrenBKVFJtY1REN3NpUGpydU5IODFocEx0VUQyZDJRTGJwWTRZY1dLMU1RdlB3VgozZGR3WXJhcklHN0JyOGlhTFFnVG9kQUpXQ3djL3dtd1pndmZvZUIvR29RRlJISFpPb2I0UzlDcTdMTDNOREhWClZPTmlpL3AxM3oyYkthd0tTbXZqS1U2RGxCbVdqdUxWajN4aQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://127.0.0.1:43447
  name: kind-training
contexts:
- context:
    cluster: kind-training
    user: user-name
  name: user-name
current-context: user-name
kind: Config
preferences: {}
users:
- name: user-name
  user:
    client-certificate: /path/to/user-k8s.crt
    client-key: /path/to/user-k8s.key


```
Or we can use `client-certificate-data` and `client-key-data` and enter the content of our certificate and key coded in `base64` especially if we don't want to hold a path for certificates

```bash
kubectl get pods --kubeconfig user-k8s-conf.yaml
Error from server (Forbidden): pods is forbidden: User "user-name" cannot list resource "pods" in API group "" in the namespace "default"
```

### Set Permissions

We will give permission to create, delete, update and list component in all namespaces for our user for that we will give him a cluster role to generate a cluster role we will use imperative command to generate our yaml configuration file

```bash
kubectl create clusterrole user-nameRole --verbs=create,update,delete,list --resource=deployments.app,pods --dry-run=client -o yaml > user-k8s-cr.yaml
```

which will produce this configuration file that we will adjut

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  creationTimestamp: null
  name: user-name-clusterRole
rules:
  - apiGroups:
      - ""
    resources:
      - pods
    verbs:
      - create
      - update
      - delete
      - list
  - apiGroups:
      - apps
    resources:
      - deployments
    verbs:
      - create
      - update
      - delete
      - list
```
to know which component belongs to which apiGroup we have to refer to the documentation https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.24/

After creating the role we have to create role binding and assign it to user or user groups to be able to user the cluster 

Samething let's generate our configuration yaml file from kubectl imperative command 
```bash
kubectl create clusterrolebinding user-name-crb --clusterrole=user-name-clusterRole --user=user-name --dry-run=client -o yaml > user-k8s-crb.yaml
```
the file is strait forward we don't need to adjust anything

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  creationTimestamp: null
  name: user-k8s-crb
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: user-name-clusterRole
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: user-name
```

##### Check permissions 

We can check our permission by executing some commands as user 
`user-name` and see if we are authorized or not for example 
```bash
kubectl get services --kubeconfig user-name-conf.yaml
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   4h39m

kubectl get nodes --kubeconfig user-name-conf.yaml
Error from server (Forbidden): nodes is forbidden: User "user-name" cannot list resource "nodes" in API group "" at the cluster scope

```
As a cluster administrator we can check permission for a user or multiple users 

```bash
kubectl auth can-i create pod --as user-name
yes
```

##### Service account

For service account it's almost the same thing for human user but rather than generating certificates a token will be generated automatically as a kubernetes secret when creating a useraccount we just need to extract it to configure our kubeconfig file then roles and role binding files

```bash
kubectl create serviceaccount gitlab --dry-run=client -o yaml > gitlab-sa.yml
```

The configuration file is very simple 

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  creationTimestamp: null
  name: gitlab
```
let's extract our token and configure our service account kubeconfig file

```bash
kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-9tcqs   kubernetes.io/service-account-token   3      21h
gitlab-token-fggnh    kubernetes.io/service-account-token   3      2m24s
```
We have our `gitlab-token-fggnh` secret and we have to print the configuration as yaml format to extract our token which is base64 encoded.

`kubectl get secret gitlab-token-fggnh -o yaml`

```yaml
apiVersion: v1
data:
  ca.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM1ekNDQWMrZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeU1ESXlNVEV4TURjME0xb1hEVE15TURJeE9URXhNRGMwTTFvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTDhJCitHRHRJMVZTTXplNk91ajF2OFpESmIxRFFidEphbEpYVFFYVDFrcGtoZ0lRamFMNDdyNXNIWHpWdDUzc1QxQkkKVjI1eDFoQjNQZjVGaWhxZ2tBcHF3ZEtJbmZYRE9vbTZ1MUp0cjk1YjFlVk5xNm4ya1dVSTJ5SndKRmVjcjNqMQpsYzVlNm8wMDlkS0NRUkMrZFcxbXBKRTU5MnpPSVo3UUlvaHFtSmpjY2pzOFVrSEFuMm4xSnpOUXdVY2h6QzVxCjY5SlZibklLQXJoV1dNUnlUdjBUS3pGNUNVc0I0TDRHUmRtWkNWYUQzalFGSitlSWhva0o5YTRlTjEzWEFjNmYKZ1dUVGJPRXNhZXRqWGlGRmZDRTdEalhJMlBkZSsrVnVhVWtyS3htMWxoenloNGtEN1R1eEZSKzFHdTdqOUlDawo2NEtrcys3RU1LSGxWc1BPMFlzQ0F3RUFBYU5DTUVBd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZNeDNMeForcUR0dVJHL1k5QXZWMkhvL2VCbjNNQTBHQ1NxR1NJYjMKRFFFQkN3VUFBNElCQVFCRjRVNE9BRG95U2x0TTBocHRrVHZJVEJVVm5yWW5ielN3SVpMRWhZckhJY2hmcmh1YQp0bUpBQXEyaTN0RVRVYWluQTFYK01yNWlrY080NTRxOW1VWkhTbTlGQ1BxU2F5OER5OUs5bzJiZlE4Y29HLzdwCkhDKzY3NWZ3WTdMeVh5bm9ScFczdTF4QlZXejE4UlZyTm1NdkdFTFZRTTZzemJNbStmYTg1R3FkWjRuMy9wZkcKUnNMWnVGQzRnSjR3dUhZcUdrenBKVFJtY1REN3NpUGpydU5IODFocEx0VUQyZDJRTGJwWTRZY1dLMU1RdlB3VgozZGR3WXJhcklHN0JyOGlhTFFnVG9kQUpXQ3djL3dtd1pndmZvZUIvR29RRlJISFpPb2I0UzlDcTdMTDNOREhWClZPTmlpL3AxM3oyYkthd0tTbXZqS1U2RGxCbVdqdUxWajN4aQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
  namespace: ZGVmYXVsdA==
  token: ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNklraDZNV2hPVDNNeVF6WmtVRXh3VVZwTWVHSjNaRkZmVFdsMlowbGxOaTFEVEhJemRYUkpTekJ4UTJNaWZRLmV5SnBjM01pT2lKcmRXSmxjbTVsZEdWekwzTmxjblpwWTJWaFkyTnZkVzUwSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXVZVzFsYzNCaFkyVWlPaUprWldaaGRXeDBJaXdpYTNWaVpYSnVaWFJsY3k1cGJ5OXpaWEoyYVdObFlXTmpiM1Z1ZEM5elpXTnlaWFF1Ym1GdFpTSTZJbWRwZEd4aFlpMTBiMnRsYmkxbVoyZHVhQ0lzSW10MVltVnlibVYwWlhNdWFXOHZjMlZ5ZG1salpXRmpZMjkxYm5RdmMyVnlkbWxqWlMxaFkyTnZkVzUwTG01aGJXVWlPaUpuYVhSc1lXSWlMQ0pyZFdKbGNtNWxkR1Z6TG1sdkwzTmxjblpwWTJWaFkyTnZkVzUwTDNObGNuWnBZMlV0WVdOamIzVnVkQzUxYVdRaU9pSXpZbUZrTnpBNFl5MDRaV1V3TFRRMlpHWXRZbUUyTUMxbU1EaGtNbVJtTXpnell6Y2lMQ0p6ZFdJaU9pSnplWE4wWlcwNmMyVnlkbWxqWldGalkyOTFiblE2WkdWbVlYVnNkRHBuYVhSc1lXSWlmUS5MU3VpT1hsdU5mNV9hV1Uyc180TnFNSEhaZnFmeDhGdjBfQ1VoUmFkVUsyTUNvX1NiUXNpYmlSSGZoNGxvZXZHUmpyNmNKSThjbVdMbmFsckNCRTFqdklsLWxwZW4wV3ctVGEzcHladFNNQzZSNlZvVEJSYXRUV3hqUHFNakhHVWFrQVhGM0Q0N21ZQ1RXOWZxSnBEM2FIZk1qLXpXZU1lbmlkaFFIUHVFQUNRNUcwQVlVNjlpNU9CLVVId2U4ZVJCSE9RVHFnMHRZN3JyUktlUlU4MVJVVjJpZ3ZDamcxQUEyZVVMQzZlTEJRSjhoVFNUcWNRakJWSngzNUVIcEtYNkJrZEdWbTZWSmdtUjQ4VHVENUVncWdTQkFyR1c5MjVDTXBPTjZlRVRPa1R4bjItS3ZYdzZIMHRCM043REpMbEdrUUdlclE3TFBhX1dnVm9tRkVyb3c=
kind: Secret
metadata:
  annotations:
    kubernetes.io/service-account.name: gitlab
    kubernetes.io/service-account.uid: 3bad708c-8ee0-46df-ba60-f08d2df383c7
  creationTimestamp: "2022-02-22T08:10:09Z"
  name: gitlab-token-fggnh
  namespace: default
  resourceVersion: "47402"
  uid: a50e963b-e8f1-4fd7-a300-4d33a45e68af
type: kubernetes.io/service-account-token
```
##### Access to the cluster

To access to the cluster with a service account we can pass options to `kubectl` command or configure a config file which is more practical

```bash
kubectl --server https://127.0.0.1:43447 \
--certificate-authority ca.cert \
--token  eyJhbGciOiJSUzI1NiIsImtpZCI6Ikh6MWhOT3MyQzZkUExwUVpMeGJ3ZFFfTWl2Z0llNi1DTHIzdXRJSzBxQ2MifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImdpdGxhYi10b2tlbi1mZ2duaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJnaXRsYWIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIzYmFkNzA4Yy04ZWUwLTQ2ZGYtYmE2MC1mMDhkMmRmMzgzYzciLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGVmYXVsdDpnaXRsYWIifQ.LSuiOXluNf5_aWU2s_4NqMHHZfqfx8Fv0_CUhRadUK2MCo_SbQsibiRHfh4loevGRjr6cJI8cmWLnalrCBE1jvIl-lpen0Ww-Ta3pyZtSMC6R6VoTBRatTWxjPqMjHGUakAXF3D47mYCTW9fqJpD3aHfMj-zWeMenidhQHPuEACQ5G0AYU69i5OB-UHwe8eRBHOQTqg0tY7rrRKeRU81RUV2igvCjg1AA2eULC6eLBQJ8hTSTqcQjBVJx35EHpKX6BkdGVm6VJgmR48TuD5EgqgSBArGW925CMpON6eETOkTxn2-KvXw6H0tB3N7DJLlGkQGerQ7LPa_WgVomFErow \
get pods
Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:default:gitlab" cannot list resource "pods" in API group "" in the namespace "default"
```
We were able to authenticate but as we didn't yet set permissions to user account we got forbidden message. Notice that service account name format is `system:serviceaccount:name-space:sa-names`

The kubeconfig file is almost similar to the configuation we already did with `user-name` previously we just replace certificates with the decoded token and the key `token`

```yaml
---
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM1ekNDQWMrZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeU1ESXlNVEV4TURjME0xb1hEVE15TURJeE9URXhNRGMwTTFvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTDhJCitHRHRJMVZTTXplNk91ajF2OFpESmIxRFFidEphbEpYVFFYVDFrcGtoZ0lRamFMNDdyNXNIWHpWdDUzc1QxQkkKVjI1eDFoQjNQZjVGaWhxZ2tBcHF3ZEtJbmZYRE9vbTZ1MUp0cjk1YjFlVk5xNm4ya1dVSTJ5SndKRmVjcjNqMQpsYzVlNm8wMDlkS0NRUkMrZFcxbXBKRTU5MnpPSVo3UUlvaHFtSmpjY2pzOFVrSEFuMm4xSnpOUXdVY2h6QzVxCjY5SlZibklLQXJoV1dNUnlUdjBUS3pGNUNVc0I0TDRHUmRtWkNWYUQzalFGSitlSWhva0o5YTRlTjEzWEFjNmYKZ1dUVGJPRXNhZXRqWGlGRmZDRTdEalhJMlBkZSsrVnVhVWtyS3htMWxoenloNGtEN1R1eEZSKzFHdTdqOUlDawo2NEtrcys3RU1LSGxWc1BPMFlzQ0F3RUFBYU5DTUVBd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZNeDNMeForcUR0dVJHL1k5QXZWMkhvL2VCbjNNQTBHQ1NxR1NJYjMKRFFFQkN3VUFBNElCQVFCRjRVNE9BRG95U2x0TTBocHRrVHZJVEJVVm5yWW5ielN3SVpMRWhZckhJY2hmcmh1YQp0bUpBQXEyaTN0RVRVYWluQTFYK01yNWlrY080NTRxOW1VWkhTbTlGQ1BxU2F5OER5OUs5bzJiZlE4Y29HLzdwCkhDKzY3NWZ3WTdMeVh5bm9ScFczdTF4QlZXejE4UlZyTm1NdkdFTFZRTTZzemJNbStmYTg1R3FkWjRuMy9wZkcKUnNMWnVGQzRnSjR3dUhZcUdrenBKVFJtY1REN3NpUGpydU5IODFocEx0VUQyZDJRTGJwWTRZY1dLMU1RdlB3VgozZGR3WXJhcklHN0JyOGlhTFFnVG9kQUpXQ3djL3dtd1pndmZvZUIvR29RRlJISFpPb2I0UzlDcTdMTDNOREhWClZPTmlpL3AxM3oyYkthd0tTbXZqS1U2RGxCbVdqdUxWajN4aQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://127.0.0.1:43447
  name: kind-training
contexts:
- context:
    cluster: kind-training
    user: gitlab
  name: gitlab
current-context: gitlab
kind: Config
preferences: {}
users:
- name: gitlab
  user:
    token: eyJhbGciOiJSUzI1NiIsImtpZCI6Ikh6MWhOT3MyQzZkUExwUVpMeGJ3ZFFfTWl2Z0llNi1DTHIzdXRJSzBxQ2MifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImdpdGxhYi10b2tlbi1mZ2duaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJnaXRsYWIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIzYmFkNzA4Yy04ZWUwLTQ2ZGYtYmE2MC1mMDhkMmRmMzgzYzciLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6ZGVmYXVsdDpnaXRsYWIifQ.LSuiOXluNf5_aWU2s_4NqMHHZfqfx8Fv0_CUhRadUK2MCo_SbQsibiRHfh4loevGRjr6cJI8cmWLnalrCBE1jvIl-lpen0Ww-Ta3pyZtSMC6R6VoTBRatTWxjPqMjHGUakAXF3D47mYCTW9fqJpD3aHfMj-zWeMenidhQHPuEACQ5G0AYU69i5OB-UHwe8eRBHOQTqg0tY7rrRKeRU81RUV2igvCjg1AA2eULC6eLBQJ8hTSTqcQjBVJx35EHpKX6BkdGVm6VJgmR48TuD5EgqgSBArGW925CMpON6eETOkTxn2-KvXw6H0tB3N7DJLlGkQGerQ7LPa_WgVomFErow
```

##### Create role and role binding for useraccount

We give the strict minimum permission to our application and a specific name space `dev-ns`, for that we won't create a cluster role but a role related to a name space

`kubectl create role gitlab-role --verb=create,update,list,get --resource=deployments.apps, services,ingress.networking.k8s.io --dry-run=client -o yaml > gitlab-role.yaml` and then we adjust in the yaml file

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: gitlab-role
  namespace: dev-ns
rules:
  - apiGroups:
      - ""
    resources:
      - services
    verbs:
      - get
      - create
      - update
  - apiGroups:
      - apps
    resources:
      - deployments
    verbs:
      - get
      - create
      - update
      - list
  - apiGroups:
      - networking.k8s.io
    resources:
      - ingresses
    verbs:
      - get
      - create
      - update

```

```
kubectl describe role -n dev-ns gitlab-ci 
Name:         gitlab-role
Labels:       <none>
Annotations:  <none>
PolicyRule:
  Resources                    Non-Resource URLs  Resource Names  Verbs
  ---------                    -----------------  --------------  -----
  deployments.apps             []                 []              [get create update list]
  services                     []                 []              [get create update]
  ingresses.networking.k8s.io  []                 []              [get create update]
```

Same thing for role binding we create the yaml file from imperative command and then we adjust configuration here we specify `dev-ns` namespace

`kubectl create rolebinding gitlab-ci-rb --role=gitlab-role --serviceaccount=default:gitlab --dry-run=client -o yaml > gitlab-rb.yaml`

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  creationTimestamp: null
  name: gitlab-rb
  namespace: dev-ns
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: gitlab-role
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: default
```

##### check permissions

```bash
kubectl auth can-i list deployments --as system:serviceaccount:default:gitlab -n dev-ns  
yes
kubectl auth can-i list deployments --as system:serviceaccount:default:gitlab -n default
no
```

## Troubleshouting

As the cluster is running and with access to different groups we can have some issues we need to troubleshoot, it could be a problem whithin the pod, is it running or not, or a configuration problem for example the service is not correctly configured, or is not forwarding request to the correct pod, network issues etc... 

So we need to know how to troubleshoot different issues in kubernetes with useful concepts like command and args and how to format `kubectl` output to make it easier to read and pinpoint the problem

#### Troubleshoot applicaitons

When troubleshooting application we start by checking if pods are running with `kubectl get pod POD_NAME` if it has a service attached to it and if the service is forwarding the request. 
We can use for that `kubectl get ep` to check all services and their endpoint or `kubectl describe SERVICE_NAME` which gives all details about service and its attached pod.

Sometimes the container fails to start so we won't be able to get container logs with `kubectl logs` command. 
We have to see the status of the container and why it's not starting this can be done with `kubectl describe pod POD_NAME`which will give us the pod's status and recent events, one of these event could give us information about why the container is not starting.

#### Debug with busybox (temporary pod)

One way to debug issues with applications and networking issues in the cluster is using temporary pods to execute commands from as pod network is different from cluster nodes. 
As these pods are temporary we can create them using imperative command `kubectl run debug-pod --image=busybox`

For busybox if we just run the pod, it will start and exit because there is no daemon running to make it running indefinitly so to keep it alive we can either start the pod in interactive mode or override the cmd or entrypoint command defined in the docker file with command and args in the yaml configuration file

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: busybox-pod
spec:
  containers:
  - name: busybox-container
    image: busybox
    command: ["sh"]
    args: ["-c", "sleep 1000"]
```

To debug we can enter the pod with `kubectl exec -it POD_NAME -- /bin/bash` and we can execute commands inside the pod.

We can also execute commands without entring the pod and without getting an interactive terminal for exemple `kubectl exec -it POD_NAME -- /bin/bash -c "ping SERVICE_NAME"`or any other command

#### kubectl formatting output

In addition to debuging from the pod we also want to debug the cluster component using `kubectl` as it's a powerful tool that gives us all those informations like IP addresses, creation time, image and container name etc... of all kubernetes components.

We can get this using the JSONPath expressions to filter on a specific fields in the JSON object and format output. 

Kubectl have several output format like standard, yaml and json format 
```bash
kubectl get pod -n metallb-system  
NAME                          READY   STATUS    RESTARTS   AGE
controller-66445f859d-jqjxn   1/1     Running   0          6m10s
speaker-72bql                 1/1     Running   0          6m10s
speaker-752b4                 1/1     Running   0          6m7s
speaker-kxgt6                 1/1     Running   0          6m6s
speaker-rw792                 1/1     Running   0          6m6s
```

```bash
kubectl get pod -n metallb-system -o wide
NAME                          READY   STATUS    RESTARTS   AGE     IP           NODE                     NOMINATED NODE   READINESS GATES
controller-66445f859d-jqjxn   1/1     Running   0          7m55s   10.244.2.2   training-worker3         <none>           <none>
speaker-72bql                 1/1     Running   0          7m55s   172.18.0.5   training-control-plane   <none>           <none>
speaker-752b4                 1/1     Running   0          7m52s   172.18.0.3   training-worker          <none>           <none>
speaker-kxgt6                 1/1     Running   0          7m51s   172.18.0.4   training-worker3         <none>           <none>
speaker-rw792                 1/1     Running   0          7m51s   172.18.0.2   training-worker2         <none>           <none>
```
```bash
kubectl get pod -n metallb-system -o yaml
```
```yaml
apiVersion: v1
items:
- apiVersion: v1
  kind: Pod
  metadata:
    annotations:
      prometheus.io/port: "7472"
      prometheus.io/scrape: "true"
    creationTimestamp: "2022-06-10T15:28:45Z"
    generateName: controller-66445f859d-
    labels:
      app: metallb
      component: controller
      pod-template-hash: 66445f859d
    name: controller-66445f859d-jqjxn
    namespace: metallb-system
    ownerReferences:
    - apiVersion: apps/v1
      blockOwnerDeletion: true
      controller: true
      kind: ReplicaSet
      name: controller-66445f859d
      uid: f7c38898-b097-463c-92cf-ebec9fd96518
    resourceVersion: "1143"
    uid: db2e1f63-26d3-49ee-9347-18f24aa12038
  spec:
    containers:
    - args:
      - --port=7472
      - --config=config
      - --log-level=info
      env:
      - name: METALLB_ML_SECRET_NAME
        value: memberlist
      - name: METALLB_DEPLOYMENT
        value: controller
      image: quay.io/metallb/controller:v0.12.1
      imagePullPolicy: IfNotPresent
      livenessProbe:
        failureThreshold: 3
        httpGet:
          path: /metrics
          port: monitoring
          scheme: HTTP
        initialDelaySeconds: 10
        periodSeconds: 10
        successThreshold: 1
        timeoutSeconds: 1
      name: controller
      ports:
      - containerPort: 7472
        name: monitoring
        protocol: TCP
      readinessProbe:
        failureThreshold: 3
        httpGet:
          path: /metrics
          port: monitoring
          scheme: HTTP
        initialDelaySeconds: 10
        periodSeconds: 10
        successThreshold: 1
        timeoutSeconds: 1
      resources: {}
      securityContext:
        allowPrivilegeEscalation: false
        capabilities:
          drop:
          - all
        readOnlyRootFilesystem: true
      terminationMessagePath: /dev/termination-log
      terminationMessagePolicy: File
      volumeMounts:
      - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
        name: kube-api-access-8555k
        readOnly: true
    dnsPolicy: ClusterFirst
    enableServiceLinks: true
    nodeName: training-worker3
    nodeSelector:
      kubernetes.io/os: linux
    preemptionPolicy: PreemptLowerPriority
    priority: 0
    restartPolicy: Always
    schedulerName: default-scheduler
    securityContext:
      fsGroup: 65534
      runAsNonRoot: true
      runAsUser: 65534
    serviceAccount: controller
    serviceAccountName: controller
    terminationGracePeriodSeconds: 0
    tolerations:
    - effect: NoExecute
      key: node.kubernetes.io/not-ready
      operator: Exists
      tolerationSeconds: 300
    - effect: NoExecute
      key: node.kubernetes.io/unreachable
      operator: Exists
      tolerationSeconds: 300
    volumes:
    - name: kube-api-access-8555k
      projected:
        defaultMode: 420
        sources:
        - serviceAccountToken:
            expirationSeconds: 3607
            path: token
        - configMap:
            items:
            - key: ca.crt
              path: ca.crt
            name: kube-root-ca.crt
        - downwardAPI:
            items:
            - fieldRef:
                apiVersion: v1
                fieldPath: metadata.namespace
              path: namespace
  status:
    conditions:
    - lastProbeTime: null
      lastTransitionTime: "2022-06-10T15:28:48Z"
      status: "True"
      type: Initialized
    - lastProbeTime: null
      lastTransitionTime: "2022-06-10T15:30:28Z"
      status: "True"
      type: Ready
    - lastProbeTime: null
      lastTransitionTime: "2022-06-10T15:30:28Z"
      status: "True"
      type: ContainersReady
    - lastProbeTime: null
      lastTransitionTime: "2022-06-10T15:28:48Z"
      status: "True"
      type: PodScheduled
    containerStatuses:
    - containerID: containerd://3d140aaafa8994db2b325f32cf3861831355174329d5a0d9b71bdc63470d7e60
      image: quay.io/metallb/controller:v0.12.1
      imageID: quay.io/metallb/controller@sha256:4db40890faff645102d33a04fbb736265fad2259425f5a01cbb84df524eae53a
      lastState: {}
      name: controller
      ready: true
      restartCount: 0
      started: true
      state:
        running:
          startedAt: "2022-06-10T15:30:10Z"
    hostIP: 172.18.0.4
    phase: Running
    podIP: 10.244.2.2
    podIPs:
    - ip: 10.244.2.2
    qosClass: BestEffort
    startTime: "2022-06-10T15:28:48Z"
  ```
```bash
kubectl get pod -n metallb-system controller-66445f859d-jqjxn -o json 
```

```json
{
    "apiVersion": "v1",
    "kind": "Pod",
    "metadata": {
        "annotations": {
            "prometheus.io/port": "7472",
            "prometheus.io/scrape": "true"
        },
        "creationTimestamp": "2022-06-10T15:28:45Z",
        "generateName": "controller-66445f859d-",
        "labels": {
            "app": "metallb",
            "component": "controller",
            "pod-template-hash": "66445f859d"
        },
        "name": "controller-66445f859d-jqjxn",
        "namespace": "metallb-system",
        "ownerReferences": [
            {
                "apiVersion": "apps/v1",
                "blockOwnerDeletion": true,
                "controller": true,
                "kind": "ReplicaSet",
                "name": "controller-66445f859d",
                "uid": "f7c38898-b097-463c-92cf-ebec9fd96518"
            }
        ],
        "resourceVersion": "1143",
        "uid": "db2e1f63-26d3-49ee-9347-18f24aa12038"
    },
    "spec": {
        "containers": [
            {
                "args": [
                    "--port=7472",
                    "--config=config",
                    "--log-level=info"
                ],
                "env": [
                    {
                        "name": "METALLB_ML_SECRET_NAME",
                        "value": "memberlist"
                    },
                    {
                        "name": "METALLB_DEPLOYMENT",
                        "value": "controller"
                    }
                ],
                "image": "quay.io/metallb/controller:v0.12.1",
                "imagePullPolicy": "IfNotPresent",
                "livenessProbe": {
                    "failureThreshold": 3,
                    "httpGet": {
                        "path": "/metrics",
                        "port": "monitoring",
                        "scheme": "HTTP"
                    },
                    "initialDelaySeconds": 10,
                    "periodSeconds": 10,
                    "successThreshold": 1,
                    "timeoutSeconds": 1
                },
                "name": "controller",
                "ports": [
                    {
                        "containerPort": 7472,
                        "name": "monitoring",
                        "protocol": "TCP"
                    }
                ],
                "readinessProbe": {
                    "failureThreshold": 3,
                    "httpGet": {
                        "path": "/metrics",
                        "port": "monitoring",
                        "scheme": "HTTP"
                    },
                    "initialDelaySeconds": 10,
                    "periodSeconds": 10,
                    "successThreshold": 1,
                    "timeoutSeconds": 1
                },
                "resources": {},
                "securityContext": {
                    "allowPrivilegeEscalation": false,
                    "capabilities": {
                        "drop": [
                            "all"
                        ]
                    },
                    "readOnlyRootFilesystem": true
                },
                "terminationMessagePath": "/dev/termination-log",
                "terminationMessagePolicy": "File",
                "volumeMounts": [
                    {
                        "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount",
                        "name": "kube-api-access-8555k",
                        "readOnly": true
                    }
                ]
            }
        ],
        "dnsPolicy": "ClusterFirst",
        "enableServiceLinks": true,
        "nodeName": "training-worker3",
        "nodeSelector": {
            "kubernetes.io/os": "linux"
        },
        "preemptionPolicy": "PreemptLowerPriority",
        "priority": 0,
        "restartPolicy": "Always",
        "schedulerName": "default-scheduler",
        "securityContext": {
            "fsGroup": 65534,
            "runAsNonRoot": true,
            "runAsUser": 65534
        },
        "serviceAccount": "controller",
        "serviceAccountName": "controller",
        "terminationGracePeriodSeconds": 0,
        "tolerations": [
            {
                "effect": "NoExecute",
                "key": "node.kubernetes.io/not-ready",
                "operator": "Exists",
                "tolerationSeconds": 300
            },
            {
                "effect": "NoExecute",
                "key": "node.kubernetes.io/unreachable",
                "operator": "Exists",
                "tolerationSeconds": 300
            }
        ],
        "volumes": [
            {
                "name": "kube-api-access-8555k",
                "projected": {
                    "defaultMode": 420,
                    "sources": [
                        {
                            "serviceAccountToken": {
                                "expirationSeconds": 3607,
                                "path": "token"
                            }
                        },
                        {
                            "configMap": {
                                "items": [
                                    {
                                        "key": "ca.crt",
                                        "path": "ca.crt"
                                    }
                                ],
                                "name": "kube-root-ca.crt"
                            }
                        },
                        {
                            "downwardAPI": {
                                "items": [
                                    {
                                        "fieldRef": {
                                            "apiVersion": "v1",
                                            "fieldPath": "metadata.namespace"
                                        },
                                        "path": "namespace"
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ]
    },
    "status": {
        "conditions": [
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2022-06-10T15:28:48Z",
                "status": "True",
                "type": "Initialized"
            },
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2022-06-10T15:30:28Z",
                "status": "True",
                "type": "Ready"
            },
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2022-06-10T15:30:28Z",
                "status": "True",
                "type": "ContainersReady"
            },
            {
                "lastProbeTime": null,
                "lastTransitionTime": "2022-06-10T15:28:48Z",
                "status": "True",
                "type": "PodScheduled"
            }
        ],
        "containerStatuses": [
            {
                "containerID": "containerd://3d140aaafa8994db2b325f32cf3861831355174329d5a0d9b71bdc63470d7e60",
                "image": "quay.io/metallb/controller:v0.12.1",
                "imageID": "quay.io/metallb/controller@sha256:4db40890faff645102d33a04fbb736265fad2259425f5a01cbb84df524eae53a",
                "lastState": {},
                "name": "controller",
                "ready": true,
                "restartCount": 0,
                "started": true,
                "state": {
                    "running": {
                        "startedAt": "2022-06-10T15:30:10Z"
                    }
                }
            }
        ],
        "hostIP": "172.18.0.4",
        "phase": "Running",
        "podIP": "10.244.2.2",
        "podIPs": [
            {
                "ip": "10.244.2.2"
            }
        ],
        "qosClass": "BestEffort",
        "startTime": "2022-06-10T15:28:48Z"
    }
}
```

The problem here like yaml that there a lot of data to scroll through, so how can we filter and extract only specific attributes we are interested in rather than having this huge yaml object for each pod. For that we can use something called JSONPath which is a query language for JSON and kubectl uses JSONPAth expressions to filter on specific fields 
JASONPath is not very easy and intuitive and may get very complex so it takes time to get used to it.

Let's for example print the name of the first pod in the metallb-system name space

```bash
kubectl get pod -n metallb-system -o jsonpath='{.items[0].metadata.name}'
controller-66445f859d-jqjxn%
```

To print the name of all pods in the namespace we add * in []

```bash
kubectl get pod -n metallb-system -o jsonpath='{.items[*].metadata.name}'
controller-66445f859d-jqjxn speaker-72bql speaker-752b4 speaker-kxgt6 speaker-rw792%
```
Here everthing is printed in a single line which is not very nice to print each pod name in a different line we add specific built-in attributes of JSONPath

```bash
kubectl get pod -n metallb-system -o jsonpath='{range .items[*]}{.metadata.name}{"\n"}{end}'
controller-66445f859d-jqjxn
speaker-72bql
speaker-752b4
speaker-kxgt6
speaker-rw792
```

Now let's get multiple information not just the pod name, for example add the IP address next to the name for that we can add a space after the name attribute with \t and add the podIP from the status section which is on the same level as metadata section

```bash
kubectl get pod -n metallb-system -o jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.status.podIP}{"\n"}{end}' 
controller-66445f859d-jqjxn     10.244.2.2
speaker-72bql   172.18.0.5
speaker-752b4   172.18.0.3
speaker-kxgt6   172.18.0.4
speaker-rw792   172.18.0.2
```
We can add other attributes like that for example let's add the start time which is on the status section also

```bash
kubectl get pod -n metallb-system -o jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.status.podIP}{"\t"}{.status.startTime}{"\n"}{end}' 
controller-66445f859d-jqjxn     10.244.2.2      2022-06-10T15:28:48Z
speaker-72bql   172.18.0.5      2022-06-10T15:28:48Z
speaker-752b4   172.18.0.3      2022-06-10T15:28:49Z
speaker-kxgt6   172.18.0.4      2022-06-10T15:28:49Z
speaker-rw792   172.18.0.2      2022-06-10T15:28:49Z
```

JSONPath is nice and let us print information in an easy readable way, but we miss each column meaning here for exemple we don't know if the name is a service name or a pod name so we can use another format which is custom-columns to group each information under a column name. The expression is also easier than jsonpath 

```bash
kubectl get pod -n metallb-system -o custom-columns="POD NAME":metadata.name,"POD IP":status.podIP,"CREATED AT":status.startTime

POD NAME                      POD IP       CREATED AT
controller-66445f859d-jqjxn   10.244.2.2   2022-06-10T15:28:48Z
speaker-72bql                 172.18.0.5   2022-06-10T15:28:48Z
speaker-752b4                 172.18.0.3   2022-06-10T15:28:49Z
speaker-kxgt6                 172.18.0.4   2022-06-10T15:28:49Z
speaker-rw792                 172.18.0.2   2022-06-10T15:28:49Z
```

## Multi-container pods

Normally a pod runs one container but sometimes we need to add some helpers to our application like a log collector for example or an init script that runs before the application. For that purpose kubernetes can run multiple containers in the same pod

We have two kinds of containers:
- Sidecar containers that run at the same time as the main container
- Init containers that run before the main container once completed the main container gets up
  
#### Sidecar container

Sidecars are containers in the same pod as the main container. Sidecar container starts simultaniously with the main container and runs as long as the pod is up. 
- Sidecars usually runs helper next to main application (log collector, synchronisation script...)
- Sidecars and main container usually operates asynchronously 
- Containers talk with each other on localhost, they don't need a service name or an ip address
- Containers can share the data among themselves

In our example of nginx deployment let's add a sidecar pod that collects logs on nginx and send them to an external server (we will simulate the logs with a simple echo command using alpine image)
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-deployment
  name: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
      - name: log-sidecar
        image: alpine
        command: ["sh"]
        args: ["-c","while true; do echo sending logs to server; sleep 10; done"]
```

```bash
kubectl get pods 
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-55967b74d8-9vlfd   2/2     Running   0          2m5s
nginx-deployment-55967b74d8-q2z9m   2/2     Running   0          2m5s
pod-with-tolerations                1/1     Running   0          70m
```

We have 2 replicas with 2 containers in each one. To see the logs and how our pod is running we have to specify the container name we want to log samething if we want to execute a shell into the container 

```bash
kubectl logs nginx-deployment-55967b74d8-9vlfd
error: a container name must be specified for pod nginx-deployment-55967b74d8-9vlfd, choose one of: [nginx log-sidecar]

kubectl logs nginx-deployment-55967b74d8-9vlfd -c log-sidecar
sending logs to server
sending logs to server
sending logs to server
sending logs to server

kubectl exec -it nginx-deployment-55967b74d8-9vlfd -c log-sidecar -- sh
/ #
```

#### Init container

As their names suggests init containers are containers that run before the main application and are used to initialize something
- Init containers run once and exit
- Main container starts afterward 

On our previous example with a sidecar let's add an init container 

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-deployment
  name: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
        - name: nginx
          image: nginx:latest
          ports:
          - containerPort: 80
        - name: log-sidecar
          image: alpine
          command: ["sh"]
          args: ["-c","while true; do echo sending logs to server; sleep 10; done"]
      initContainers:
        - name: mydb-available
          image: busybox
          command: ["sh"]
          args: ["-c","until nslookup mydb-service; do echo waiting for database to start; sleep 3; done"]
```

```bash
kubectl get pods
NAME                                READY   STATUS     RESTARTS   AGE
nginx-deployment-5df48d6749-46jn4   0/2     Init:0/1   0          8s
nginx-deployment-5df48d6749-kcssc   0/2     Init:0/1   0          8s
pod-with-tolerations                1/1     Running    0          97m

kubectl logs nginx-deployment-5df48d6749-tnhvk -c mydb-available

Server:         10.96.0.10
Address:        10.96.0.10:53

** server can't find mydb-service.default.svc.cluster.local: NXDOMAIN

*** Can't find mydb-service.svc.cluster.local: No answer
*** Can't find mydb-service.cluster.local: No answer
*** Can't find mydb-service.default.svc.cluster.local: No answer
*** Can't find mydb-service.svc.cluster.local: No answer
*** Can't find mydb-service.cluster.local: No answer

waiting for database to start
```

#### Access informations about pods and cluster

Every kubernetes component definition has a list of key:value pair that can be read and used as enviroment variables so let's retrieve some information from our running pods and display them on the log-sidecar for that we will modify the yaml file and some attributes like `env`, `valueFrom`, `fieldRef` and `fieldPath`

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-deployment
  name: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deployment
  template:
    metadata:
      labels:
        app: nginx-deployment
    spec:
      containers:
        - name: nginx
          image: nginx:latest
          ports:
          - containerPort: 80
        - name: log-sidecar
          image: alpine
          command: ["sh"]
          args: 
            - -c,
            - while true; 
              do 
                echo sending logs to server;
                printenv POD_NAME POD_IP NODE_NAME
                sleep 10; 
              done
          env:
            - name: POD_NAME
                valueFrom:
                  fieldRef:
                    fieldPath: metadata.name
            - name: POD_IP
                valueFrom:
                  fieldRef:
                    fieldPath: status.podIP
            - name: NODE_NAME
                valueFrom:
                  filedRef:
                    fieldPath: spec.nodeName
```

available fields for `fieldRef` can be found in kubernetes documentation at https://kubernetes.io/docs/concepts/workloads/pods/downward-api/#available-fields

## Persisting Data

Pods are ephemeral and can be recreated at anytime so when a pod crashes or a node disconnect another pod is scheduled and started so any modification done before the crash is lost as the container starts from its initial image. To prvent data loss kubernetes can persist data with different components that are persistent volumes, persistent volume claim and storage class

But kubernetes doesn't offer data persistence out of the box we have to configure volumes that doesn't depend on pod life cycle niether on a node life cycle.
The configured volume must be reachable and accessible from any node so when a pod restarts it can find and read data from that location and get updated.
So to summerize storage for data persistance must be
- independent from pod life cycle
- available on all nodes
- survive even if cluster crashes

3 compoonents will be covered for data persistence
- persistent volume
- persistent volume claim
- storage class

### Persistent Volumes

Let's consider we have a mysql database in which data is updated, added etc.... when the pod dies, all changes will be lost. We must have a storage in which the data will be stored and when the pod is recreated it reads the content of that same storage and get updated data. But we don't know in which node the pod will be recreated that's why our storage must be accessible form all nodes

Another use case, when we have a directory, for example our application reads and writes data to a specific preconfigured directory like session files or configuration files etc...

For these two use cases we can persist data using kubernetes persistent volume component or pv

Persistent volume is 
- Cluster resource that is used to store data
- Created using YAML configuration files
  - in which we specify the `kind` which is `PersistentVolume`
  - spec: example how much storage
- Persistent volume is an abstract component it must take the storage from the actual physical storage which can be
  - Local hard drive
  - External nfs storage
  - Cloud storage

The question here is where does the storage come from and who make it available to the cluster ?

Kubernetes doesn't care about the real storage it gives persistent volume component as an interface to the real storage that we need to create and manage by ourselves. So we have to decide what type of storage our cluster and applications need.
We can have different types of storage on the same cluster and applications can also use multiple types of storage

example of nfs storage backend

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0003
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: slow
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /tmp
    server: 172.17.0.2
```

Depending on the type of storage specifications can change for example a local storage specifications would be like below

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: example-pv
spec:
  capacity:
    storage: 100Gi
  volumeMode: Filesystem
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Delete
  storageClassName: local-storage
  local:
    path: /mnt/disks/ssd1
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - example-node
```

Note that `PersistentVolumes` are not namespaced they are available to the whole cluster

#### Local Vs Remote Volume Types

It's important to differnciate between the two categories of volumes
- Each one has it's own use case
- Local volume types violate the 2nd and 3rd requirement for data persistence
  - Being tied to a specific node
  - Doesn't survive to cluser or node crash

#### Kubernetes Administrators and Users

Persistent volumes are resources like RAM and CPU that must be already available before the pod that depends or uses it is created so for that we 2 types of users 
- Kubernetes administrators 
  - Set up and maintains the cluster
  - Make sure that the cluster has enough resources
  - Usually System administrators or devops in a company
- Kubernetes Users
  - Deploy applications into the cluster
  - Usually developers

So the kubernetes administrator is the one who configure storages that will be available to the cluster and create persistent volume component from these storage backends. 
Users will then be able to use that storage and they must explicitly configure the application for that, so the application havs to claim the persistent volume

### Persistent Volume Claim

`PersistentVolumeClaim` is another component that is also created with yaml configuration
- PVC claims a volume with a certain storage size or capacity which defined is `PersistentVolumeClaim` and some other charectiristics like access mode

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-name
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 10Gi
```
- The claim has to be used in the pod definition

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: pvc-name
```
`claimName` must `PersistentVolumeClaim` name 

#### Volume abstraction

- Pod request a volume through the PVC
- Claim tries to find a volume in the cluster that satisfies the claim
- The volume will have a storage that actual storage backend that will create that storage resource from
- The pod will be able to use the real storage backend
- Persistent Volume Claim are namespaced and must be in the same namespace than the pod
- The volume is mounted into the pod and the volume can be mounted in the container inside the pod
- The container can read and write to that storage
- When the pod dies and a new one is created it will see the same storage and has access to the same data than the previous one

### ConfigMap and Secret

`ConfigMap` and `Secret` are different from PersistentVolume component they are
- Local volumes
- Not created via PV and PVC
- Managed by Kubernetes

let's consider a case when we need a configuration file for our application or an SSL certificate mounted inside the application so we need a file to be available for our pod for that
- We create a `ConfigMap` and/or `Secret` component
- We mount that into our pod/container the same way as we would mount `PersistentVolumeClaim`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: busybox-container
    image: busybox
    volumeMounts:
    - name: config-dir
      mountPath: "/etc/config
      readOnly: true
  volumes:
  - name: config-dir
    configMap:
      name: busybox-configmap
```

#### To summerize

- Volume is a directory with some data 
- These volumes are accessible to containers in a pod
- How these directories are made available ? Backed by which storage medium ?
  - This is defined by specific volume types

An application can have access to multiple volume types at the same time, for example we have an elastic search application which needs configuration file, certificates and a database, we can configure `PersistentVolume`, `ConfigMap` and `Secret` at the same time and even different types of storage

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: elastic
spec:
  selector:
    matchLabels:
      app: elastic
  template:
    metadata:
      labels:
        app: elastic
    spec:
      containers:
        - image: elastic:latest
          name: elastic-container
          ports:
            - containerPort: 9200
          volumeMounts:
            - name: es-persistent-storage
              mountPath: /var/lib/data
            - name: es-secret-dir
              mountPath: /var/lib/secret
            - name: es-config-dir
              mountPath: /var/lib/config
    volumes:
      - name: es-persistent-storage
        persistentVolumClaim:
          ClaimName: es-pv-claim
      - name: es-secret-dir
        secret:
          secretName: es-secret
      - name: es-config-dir
        configMap:
          name: es-config-map
```

### Storage Class

Creating a PersistentVolume manually each time developers need storage on an application can become very quickly tidius because if we have a hundred or a thousand of applications deployed by different teams each one has its needs and the admin have to create the right volume type for the right application

For that another data persistence component is used which storage class

- Storage Class provisions `PersistentVolumes` dynamically whenever PVC claims it
- Creating and provisioning volumes in a cluster can be automated
- SotrageClass are created using YAML configuration file

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: storage-class-name
provisioner: kubernetes.io/aws-ebs
parameters:
  type: gp2
  iopsPerGB: "10"
  fsType: ext4
```

So `StorageClass` is aonther abstraction level that abstracts the underline storage provider as well as parameters for the storage like disk type for example
So how `StorageClass` is used in the pod configuration ? Same as persistent volume it's requested or claimed by PVC so in PVC configuration we add an additional attribute called `StorageClassName` taht references the storage class to be used to create a persistent that satisfies the claims of these PVC

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: claim1
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: storage-class-name
  resources:
    requests:
      storage: 30Gi
```

## Resource request and Limits

Workloads must be configured correctly and must have limitations and resources requests to not over use hardware resources and impact other applications. For that kubernetes gives tools to add limits and require resources for pods to run smoothly

### Resource request
Some applications may need more CPU or more RAM than other applications so we can define minimum resources needed to get the application working fine.
- Request is what the container is garanteed to get
- k8s scheduler uses this information to figure out where to run the pods

### Limits
Limits are important because sometimes an application can use more than requested resources so if it's allowed to exceed the allocated resources it could impact the rest of the applications which won't have enough resources
So limits are here to make sure that
- Container never goes above a certain value
- Container is only allowed to go up the limit

#### Configuring Requests and limits

Limitations and Requests are configured in yaml files

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: frontend
spec:
  containers:
  - name: app
    image: images.my-company.example/app:v4
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
  - name: log-aggregator
    image: images.my-company.example/log-aggregator:v6
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```

## Scheduling pods

Pods run on any available node if resources are available but sometimes we have an application that has specific requirements so we want to run it on a node that matches these requirements for example an intensive CPU application must run on nodes that are configured for intensive treatment we can do that in a multiple ways

### Node Name and Node Selector

#### Node Name

`NodeName` is the simplest way to define a pod in which we want a pod to be scheduled we assign a specific node in which the pod will be scheduled this can be very simple and practical with small clusters, but will be tidius with dynamic and huge clusters

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: nginx
  name: nginx-pod
spec:
  containers:
    - image: nginx
      name: nginx-app
  nodeName: training-worker03
```

#### Node Selector
`nodeName` force pod to be scheduled on a specific node but what happens if the actual node doesn't have enough resources or if we don't know the node name, or if we have a dynamic cluster so names change.
`nodeSelector` provides a way to allow more dynamic values using labels 
- We attach a label to nodes
- Add nodeSelector field to pod configuration

```bash
kubectl get nodes --show-lables
NAME                     STATUS   ROLES                  AGE     VERSION   LABELS
training-control-plane   Ready    control-plane,master   4h24m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,ingress-ready=true,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-control-plane,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node-role.kubernetes.io/master=,node.kubernetes.io/exclude-from-external-load-balancers=
training-worker          Ready    <none>                 4h23m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker,kubernetes.io/os=linux
training-worker2         Ready    <none>                 4h23m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker2,kubernetes.io/os=linux
training-worker3         Ready    <none>                 4h23m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker3,kubernetes.io/os=linux
```
Some labels are already configured, let's add a new label on one of the nodes for example a high CPU node we will add a label `cpu=high`

```bash
kubectl label node training-worker3 cpu=high
node/training-worker3 labeled

kubectl get nodes --show-labels
NAME                     STATUS   ROLES                  AGE     VERSION   LABELS
training-control-plane   Ready    control-plane,master   4h29m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,ingress-ready=true,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-control-plane,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node-role.kubernetes.io/master=,node.kubernetes.io/exclude-from-external-load-balancers=
training-worker          Ready    <none>                 4h28m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker,kubernetes.io/os=linux
training-worker2         Ready    <none>                 4h28m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker2,kubernetes.io/os=linux
training-worker3         Ready    <none>                 4h28m   v1.21.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,cpu=high,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker3,kubernetes.io/os=linux
```

We create our pod that will be scheduled on worker3 as we will indicate cpu=high as `nodeSelector`

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: nginx
  name: nginx-with-selector
spec:
  containers:
    - image: nginx
      name: nginx-with-selector
  nodeSelector:
    cpu: high
```

```bash
kubectl get pods -o wide
NAME                  READY   STATUS    RESTARTS   AGE   IP           NODE               NOMINATED NODE   READINESS GATES
nginx-with-selector   1/1     Running   0          45s   10.244.3.4   training-worker3   <none>           <none>
```

### Node Affinity

Node Selector is more flexible than node name but has its drawbacks
- if not enough resources, pods won't be scheduled
- We can't use more flexible expression to select the proper node
- Not useful if we have thousands of nodes in different zones etc...

Node Affinity is similar to Node Selector but 
- affinity language is more expressive 
- matching labels is more flexible with operators
- syntax a bit more complex

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-with-tolerations
  name: pod-with-tolerations
spec:
  containers:
    - image: nginx
      name: pod-with-tolerations
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: kubernetes.io/hostname
                operator: In
                values:
                  - training-worker2
```

- `requiredDuringSchedulingIgnoredDuringExecution` is hard affinity and must be met pod is not scheduled
- `preferredDuringSchedulingIgnoredDuringExecution` is soft affinity and if not met the pod is scheduled elsewhere

We can have negative affinity to avoid specific nodes with `not In` operator and `DoesNotExist` operator
Supported operators are:
- In
- not In
- Exists
- DoesNotExist
- Gt
- Lt

### Taints and Tolerations

#### Taints
Taints are used to tell nodes which pods are allowed to be scheduled on them or not this what make the master node for example from not running any node on it

This taint is created when kubeadm bootstraps the cluster

```bash
kubectl describe node training-control-plane |grep Taint 
Taints:             node-role.kubernetes.io/master:NoSchedule
```
To create a taint on a node we use `kubctl taint node <node name> <key>=<value>:effect` command 

```bash
kubectl taint node training-worker3 cpu=high:NoSchedule 
node/training-worker3 tainted

kubectl describe node training-worker3 |grep Taints
Taints:             cpu=high:NoSchedule
```


#### Tolerations
Even if there is a taint on master-node some pods are scheduled anyway in master-node like etcd, coredns etc... those pods have toleration regarding configured taint which means they are not repelled by the tainted node but any other pod won't be scheduled on master node or any tainted node if tolerations are not configured. Pods will be scheduled on another pod and if not available they will have a `pending` status.

Effects: 
- NoSchedule : Pods won't be scheduled, if a pod is already running without a toleration it will still run
- NoExecute: Pods without a toleration that are already running will be killed and rescheduled

Notice that `nodeName` has precedence on any taints and/or tolerations

Tolerations are configured in the pod yaml configuration

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-with-tolerations
  name: pod-with-tolerations
spec:
  containers:
    - image: nginx
      name: pod-with-tolerations
  tolerations:
     - key: "cpu"
       operator: "Equal"
       value: "high"
       effect: "NoSchedule"
```

We can use operator `Exists` to specify only a key or an effect and omit other attributes for example 

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-with-tolerations
  name: pod-with-tolerations
spec:
  containers:
    - image: nginx
      name: pod-with-tolerations
  tolerations:
     - effect: "NoSchedule"
       operator: Exists
```
### Iner-Pod Affinity

If we have a dynamic infrastructure in which nodes are added and removed based on workload and we want to schedule an application on master node that collects master nodes logs with 5 replicas each one on an eventual added master node dynamically (we consider we have at maximum 5 masters but at any time we have between 1 and 5 masters depending on workload)

If for example we have a signle master-node, the defined 5 replicas will run on the same master node and if any node is added no replica will be scheduled because they are already running and we want to have only one replica per node

For that last statement we have the Inter-Pod Anti-affinity rules which
- Allow us to constrain which node our pod is eligible to be scheduled based on labels on pods that are already running on the node
- Prevent from running the pod on a node which has already a replica running on it

With this Anti-affinity rule we should also have an Inter-Pod Affinity rule, let's say that our application should run with etcd pod, and etcd is not configured on all master nodes, so if there no etcd pod there is no reason for our pod to be scheduled on that master, Inter-Pod Affinity rule is used for that purpose

To illustrate how it works we will first add a new label for worker3

`kubectl label node training-worker3 node-role.kubernetes.io/worker=`

```bash
kubectl get node training-worker3 --show-labels

NAME               STATUS   ROLES    AGE   VERSION   LABELS
training-worker3   Ready    worker   34m   v1.24.0   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=training-worker3,kubernetes.io/os=linux,node-role.kubernetes.io/worker=
```

Then we add a taint based on the created label to accept only desired pod

`kubectl taint node node-role.kubernetes.io/worker:NoSchedule`

We create a redis pod with affinity and toleration to worker3

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: redis
  name: redis
spec:
  containers:
    - image: redis
      name: redis
  tolerations:
    - effect: NoSchedule
      operator: Exists
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: node-role.kubernetes.io/worker
                operator: Exists
```

```bash
kubectl apply -f redis.yaml
pod/redis created
```
```bash
kubectl get pods -o wide
NAME    READY   STATUS    RESTARTS   AGE   IP           NODE               NOMINATED NODE   READINESS GATES
redis   1/1     Running   0          8s    10.244.2.4   training-worker3   <none>           <none>
```
Our redis pod has been scheduled as desired on worker3. Now we will add a deployment to run only on nodes with a redis pod running with 3 replicasets and only one replica per node

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
spec:
  selector:
    matchLabels:
      app: myapp
  replicas: 3
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
        - name: myapp-container
          image: nginx:1.20
      tolerations:
        - effect: NoSchedule
          operator: Exists
      nodeSelector:
        kubernetes.io/hostname: training-worker3
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - myapp
              topologyKey: "kubernetes.io/hostname"
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - redis
              topologyKey: "kubernetes.io/hostname"
```

We used `nodeSelector` to select worker3 in this example but we could have used `nodeAffinity` for more flexibility

```bash
kubectl get pods -o wide
NAME                                READY   STATUS    RESTARTS   AGE     IP           NODE               NOMINATED NODE   READINESS GATES
myapp-deployment-6b56fc789f-24d7n   1/1     Running   0          58s     10.244.2.5   training-worker3   <none>           <none>
myapp-deployment-6b56fc789f-2dpv5   0/1     Pending   0          58s     <none>       <none>             <none>           <none>
myapp-deployment-6b56fc789f-96zrz   0/1     Pending   0          58s     <none>       <none>             <none>           <none>
redis                               1/1     Running   0          7m36s   10.244.2.4   training-worker3   <none>           <none>
```

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp-deployment
spec:
  selector:
    matchLabels:
      app: myapp
  replicas: 3
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
        - name: myapp-container
          image: nginx:1.20
      tolerations:
        - effect: NoSchedule
          operator: Exists
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - myapp
              topologyKey: "kubernetes.io/hostname"
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - redis
              topologyKey: "kubernetes.io/hostname"
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchExpressions:
                  - key: node-role.kubernetes.io/worker
                    operator: Exists
```
```bash
kubectl apply -f podAffinity.yaml
deployment.apps/myapp-deployment created

kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
myapp-deployment-6f5dc57b79-7lvc4   0/1     Pending   0          6s
myapp-deployment-6f5dc57b79-gwpg6   0/1     Pending   0          6s
myapp-deployment-6f5dc57b79-x8sd6   1/1     Running   0          6s
redis                               1/1     Running   0          31m
```

We get the same result one replica running on node3 which has a redis pod running and because of `taint`, `tolerations` and `affinity` everything is scheduled on node `training-worker3`

## Health Check

Sometimes our pod can be running but the application is not accessible due to container crash, kubernetes restarts or reschedule any pod who crashes but if the pod is ok and the container has a problem without additional options we won't be able to see that the pod has a problem directly.
For that Liveness Probe and Readiness Probe can be configured to check the container state and if there is any issue restart the pod automtically

### Liveness Probe

Liveness Probe will check our application inside the pod we can have 3 ways for health check
- Exec Probes: Specify a command to check helth
- TCP Probes: kubelet makes probe connection at the node 
- HTTP Probes: kubelet sends and HTTP request to a specified port and path

### Readiness Probe

Readiness Probe is exactly the same than Liveness Probe the difference is that Readiness Probe checks health during startup and Liveness Probe checks health after startup. For exampel if an application takes 60 seconds to run with readiness porbe the pod will stay during these 60s in not ready state until check is OK so we won't get any error from our application during that time.

```yaml
---
apiVersion: v1
kind: Pod
metadata:
 name: myapp-health-probes
spec:
  containers:
    - image: nginx:1.20
      name: myapp-container
      ports:
        - containerPort: 80
      readinessProbe:
        tcpSocket:
          port: 80
        initialDelaySeconds: 10
        periodSeconds: 5
      livenessProbe:
        tcpSocket:
          port: 80
        initialDelaySeconds: 5
        periodSeconds: 15
```

## Deployment Strategies: Rolling updates

When deploying applications what will happen if there is a new version ? How updates are processed ? How to prevent applications downtime when an update is rolled up and how to go back to previous version if there is any problem after deploying new version.

We can also notice when creating a deployment that another component is automatically created which is replicaset


```bash
kubectl get all

NAME                                    READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-76cf68c6b6-46pkw   1/1     Running   0          2m14s
pod/nginx-deployment-76cf68c6b6-htjzv   1/1     Running   0          2m15s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   35m

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   2/2     2            2           2m15s

NAME                                          DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-deployment-76cf68c6b6   2         2         2       2m15s
```

### ReplicatSet

When we create a deployment, it creates an application rollout and for that it uses a component called ReplicaSet.
ReplicaSet is created automtically in the background and is responsible for creating the replicas and making sure that desired pod replicas are always running

Working with deployments we first
- Create deployment with configuration file
- k8s creates ReplicaSet 
- ReplicaSet creates pods

We don't have to interact with replicatset and pods directly deployments take car of those components

### Deployment update strategies

Let's say we created a deployment with version 1.0 of our application 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: myapp
  name: myapp
spec:
  replicas: 2
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - image: myapp:1.0
        name: myapp
        ports:
          - ContainerPort: 80
```

The deployment will result of the creation of a ReplicaSet that will creates pods and desired pods replicas. If we update our application image with myapp:1.2 for example a new ReplicaSet will be created and deploys the new pods with new image version the old ReplicaSet will remain and terminates pods with the old image.
But how this update happens ? In which order pods are created and terminated ? This is called update strategy and in kubernetes we have two strategies
- Recreate strategy : All existing Pods are killed before new ones are created which results in application downtime
- Rolling Update Strategy : This strategy prevents application downtime as it deletes an old Pod and creates a new one untill all Pods are deleted and replaced with new ones

Rolling update strategy is the default strategy in kubernetes and in each strategy the old ReplicaSets remains in the cluser with 0 pods running 

for example we create a deployment  with nginx:1.22 image

`kubectl create deployment mynginx-app --image=nginx:1.22 --replicas=3`

```bash
kubectl get all
NAME                               READY   STATUS    RESTARTS   AGE
pod/mynginx-app-7c94cd8665-dc26l   1/1     Running   0          27s
pod/mynginx-app-7c94cd8665-rjkqt   1/1     Running   0          27s
pod/mynginx-app-7c94cd8665-tdw9d   1/1     Running   0          27s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   102m

NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/mynginx-app   3/3     3            3           27s

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/mynginx-app-7c94cd8665   3         3         3       27s
```

We created a deployment `mynginx-app`. The deployment creates a replicaset with ID 7c94cd8665 and the replicaset creates and runs desired pods 

```
NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/mynginx-app-7c94cd8665   3         3         3       27s
```

Now we will update our deployment wiht new nginx image 

`kubectl set image deployments mynginx-app nginx=nginx:1.23 #nginx here is the name of our container`

```bash
kubectl get all

NAME                               READY   STATUS        RESTARTS   AGE
pod/mynginx-app-7bfb594854-7k8ps   1/1     Running       0          15s
pod/mynginx-app-7bfb594854-lknfb   1/1     Running       0          12s
pod/mynginx-app-7bfb594854-sf6xv   1/1     Running       0          5s
pod/mynginx-app-7c94cd8665-dc26l   1/1     Terminating   0          8m23s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   110m

NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/mynginx-app   3/3     3            3           8m23s

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/mynginx-app-7bfb594854   3         3         3       15s
replicaset.apps/mynginx-app-7c94cd8665   0         0         0       8m23s
```

As we can see old pods are terminating a new replicaset with new ID has been created and the old one with ID 7c94cd8665 remains in the cluster

#### Setting up update strategies

Update strategy is configured into the deployment configuration file. We can see which strategy is applied by inspecting the deployment which has a `RollingUpdateStrategy` and `StrategyType` section

```bash
kubectl describe deployment mynginx-app 
Name:                   mynginx-app
Namespace:              default
CreationTimestamp:      Mon, 11 Jul 2022 10:21:21 +0100
Labels:                 app=mynginx-app
Annotations:            deployment.kubernetes.io/revision: 2
Selector:               app=mynginx-app
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=mynginx-app
  Containers:
   nginx:
    Image:        nginx:1.23
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   mynginx-app-7bfb594854 (3/3 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  49m   deployment-controller  Scaled up replica set mynginx-app-7c94cd8665 to 3
  Normal  ScalingReplicaSet  41m   deployment-controller  Scaled up replica set mynginx-app-7bfb594854 to 1
  Normal  ScalingReplicaSet  41m   deployment-controller  Scaled down replica set mynginx-app-7c94cd8665 to 2
  Normal  ScalingReplicaSet  41m   deployment-controller  Scaled up replica set mynginx-app-7bfb594854 to 2
  Normal  ScalingReplicaSet  41m   deployment-controller  Scaled down replica set mynginx-app-7c94cd8665 to 1
  Normal  ScalingReplicaSet  41m   deployment-controller  Scaled up replica set mynginx-app-7bfb594854 to 3
  Normal  ScalingReplicaSet  41m   deployment-controller  Scaled down replica set mynginx-app-7c94cd8665 to 0
  ```

To modify update strategie we have to modify `strategy` section in deployment `spec` on RollingUpdate strategy we can specify the purcentage of running pods and unavailable pods to not having to wait one by one which could make the process taking a lot of time. For `Recreate` strategy we specify only `type: Recreate`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: mynginx-app
  name: mynginx-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: mynginx-app
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: mynginx-app
    spec:
      containers:
      - image: nginx:1.23
        name: nginx
        ports:
          - containerPort: 80
```

#### Rollout History

- When a Deployment Rollout is triggered, a new Deployment revision is created
- A new revision is only created when Deployment's Pod template is modified

```bash
kubectl rollout history deployment mynginx-app
deployment.apps/mynginx-app 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
```
We have two revisions for our deployment

#### Rollback

Revisions creation gives us a history of the changes made to our deployment and the opportunity to rollback to a previous version if an update causes issues

Rollback command is `kubectl rollout undo deployment <deployment-name>`

For our case we have 3 revisions and our current image is nginx:1.20

```bash
kubectl rollout history deployment mynginx-app

deployment.apps/mynginx-app
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
3         <none>
```

```bash
kubectl describe pod mynginx-app-64df86447-5v6zd

Name:         mynginx-app-64df86447-5v6zd
Namespace:    default
Priority:     0
Node:         training-worker2/172.19.0.3
Start Time:   Mon, 11 Jul 2022 11:32:30 +0100
Labels:       app=mynginx-app
              pod-template-hash=64df86447
Annotations:  <none>
Status:       Running
IP:           10.244.2.10
IPs:
  IP:           10.244.2.10
Controlled By:  ReplicaSet/mynginx-app-64df86447
Containers:
  nginx:
    Container ID:   containerd://912f9fdce12ce9dc6a348e193485565e0bebd67ce0e7cffec4236ec079a30d5a
    Image:          nginx:1.20
    Image ID:       docker.io/library/nginx@sha256:38f8c1d9613f3f42e7969c3b1dd5c3277e635d4576713e6453c6193e66270a6d
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Mon, 11 Jul 2022 11:33:09 +0100
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-vp682 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-vp682:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  16m   default-scheduler  Successfully assigned default/mynginx-app-64df86447-5v6zd to training-worker2
  Normal  Pulling    16m   kubelet            Pulling image "nginx:1.20"
  Normal  Pulled     16m   kubelet            Successfully pulled image "nginx:1.20" in 21.111810926s
  Normal  Created    16m   kubelet            Created container nginx
  Normal  Started    16m   kubelet            Started container nginx
```
```bash
kubectl rollout undo deployment mynginx-app
deployment.apps/mynginx-app rolled back

kubectl rollout status deployment mynginx-app
deployment "mynginx-app" successfully rolled out

kubectl rollout history deployment mynginx-app
deployment.apps/mynginx-app 
REVISION  CHANGE-CAUSE
1         <none>
3         <none>
4         <none>
```

Here we get back to revision 2 and revision 4 was created with the old revision 2 parameters we can add CHANGE-CAUSE with annotations in yaml configuration file and the previous replica set with ID 7bfb594854 get pods running again 

```bash
kubectl get rs
NAME                     DESIRED   CURRENT   READY   AGE
mynginx-app-64df86447    0         0         0       24m
mynginx-app-7bfb594854   3         3         3       87m
mynginx-app-7c94cd8665   0         0         0       95m
```

## ETCD backup and restore

Backing up data and cluster is critical in kubernetes we can manage backup in different ways 
- Having all our configuration files (manifests) in a version control repository so we can deploy our application if any problem occurs
- Save configuration in a single yaml file from the cluster `kubectl get all --all-namespaces -o yaml > mybackupconfig.yaml`

the problem with these methods is that we can't save every component and everything.
Another way is backing up ETCD database in which all our cluster configuration is stored. ETCD provide tools to take a snapshot of our database and restore it if something happens

ETCD can be installed as a pod into `contorl-plane` node or as a separate server (a cluster of 5 servers is recommanded for productions), for management the command `etcdctl` is available and this is what will be used to backup our cluster. (if not available it can easily be installed from etcd github repo)

### Managing and backuping ETCD

etcdctl allows us to manage our ETCD cluster like adding or removing cluster members, checking cluster health and status, taking snapshot, restoring database etc... For communication each member of the cluster uses certificates that must be specifed to use `etcdctl` otherwise the connection will fail we must also specify `ETCDCTL_API` version 2 or 3 before managing the cluster
We will assume that ETCD is installed within the `control-plane` as a static pod the configuration file is located at `/etc/kubernetes/manifests/etcd.yaml` from which we can get all required informations like certificates, data directory and endpoints in the pod specifications `spec:` and `command:` key

```yaml
apiVersion: v1
kind: Pod
metadata:
  annotations:
    kubeadm.kubernetes.io/etcd.advertise-client-urls: https://172.19.0.5:2379
  creationTimestamp: null
  labels:
    component: etcd
    tier: control-plane
  name: etcd
  namespace: kube-system
spec:
  containers:
  - command:
    - etcd
    - --advertise-client-urls=https://172.19.0.5:2379
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt
    - --client-cert-auth=true
    - --data-dir=/var/lib/etcd
    - --experimental-initial-corrupt-check=true
    - --initial-advertise-peer-urls=https://172.19.0.5:2380
    - --initial-cluster=training-control-plane=https://172.19.0.5:2380
    - --key-file=/etc/kubernetes/pki/etcd/server.key
    - --listen-client-urls=https://127.0.0.1:2379,https://172.19.0.5:2379
    - --listen-metrics-urls=http://127.0.0.1:2381
    - --listen-peer-urls=https://172.19.0.5:2380
    - --name=training-control-plane
    - --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt
    - --peer-client-cert-auth=true
    - --peer-key-file=/etc/kubernetes/pki/etcd/peer.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    - --snapshot-count=10000
    - --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    image: k8s.gcr.io/etcd:3.5.3-0
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /health
        port: 2381
        scheme: HTTP
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    name: etcd
    resources:
      requests:
        cpu: 100m
        memory: 100Mi
    startupProbe:
      failureThreshold: 24
      httpGet:
        host: 127.0.0.1
        path: /health
        port: 2381
        scheme: HTTP
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    volumeMounts:
    - mountPath: /var/lib/etcd
      name: etcd-data
    - mountPath: /etc/kubernetes/pki/etcd
      name: etcd-certs
  hostNetwork: true
  priorityClassName: system-node-critical
  securityContext:
    seccompProfile:
      type: RuntimeDefault
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/etcd
      type: DirectoryOrCreate
    name: etcd-certs
  - hostPath:
      path: /var/lib/etcd
      type: DirectoryOrCreate
    name: etcd-data
```
For example check cluster health: 
```bash
export ETCDCTL_API=3 
etcdctl endpoint health --write-out=table --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key 
+------------------------+--------+-------------+-------+
|        ENDPOINT        | HEALTH |    TOOK     | ERROR |
+------------------------+--------+-------------+-------+
| https://127.0.0.1:2379 |   true | 20.424459ms |       |
+------------------------+--------+-------------+-------+
```
Cluster status to see who is the leader members IDs, database size etc....

```bash
root@training-control-plane:~# etcdctl endpoint status --write-out=table --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key 
+------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
|        ENDPOINT        |        ID        | VERSION | DB SIZE | IS LEADER | IS LEARNER | RAFT TERM | RAFT INDEX | RAFT APPLIED INDEX | ERRORS |
+------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
| https://127.0.0.1:2379 | 4c95f7dc5897214a |   3.5.3 |  3.3 MB |      true |      false |         2 |       2674 |               2674 |        |
+------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
```

If we have a cluser with 3 nodes for example to check health, status and members we must specify all endpoints

```bash
export ETCDCTL_API=3
/usr/local/bin/etcdctl --write-out=table --endpoints https://10.10.10.11:2379,https://10.10.10.12:2379,https://10.10.10.13:2379 --cacert /etc/etcd/ssl/etcd-root-ca.pem --cert /etc/etcd/ssl/etcd01.pem --key /etc/etcd/ssl/etcd01-key.pem member list
+------------------+---------+--------+--------------------------+-------------------------+------------+
|        ID        | STATUS  |  NAME  |       PEER ADDRS         |      CLIENT ADDRS       | IS LEARNER |
+------------------+---------+--------+-------------------------+-------------------------+------------+
| 50d028ea1a5a2d7f | started | etcd03 | https://10.10.10.13:2380 | https://10.1.11.13:2379 |      false |
| 6ed45862bf8c4ba4 | started | etcd01 | https://10.10.10.11:2380 | https://10.1.11.11:2379 |      false |
| ae8b87e4bc780d43 | started | etcd02 | https://10.10.10.12:2380 | https://10.1.11.12:2379 |      false |
+------------------+---------+--------+--------------------------+-------------------------+------------+

/usr/local/bin/etcdctl --write-out=table --endpoints https://10.10.10.11:2379,https://10.10.10.12:2379,https://10.10.10.13:2379 --cacert /etc/etcd/ssl/etcd-root-ca.pem --cert /etc/etcd/ssl/etcd01.pem --key /etc/etcd/ssl/etcd01-key.pem endpoint health
+--------------------------+--------+-------------+-------+
|        ENDPOINT          | HEALTH |    TOOK     | ERROR |
+--------------------------+--------+-------------+-------+
| https://10.10.10.11:2379 |   true | 31.106475ms |       |
| https://10.10.10.12:2379 |   true | 43.705712ms |       |
| https://10.10.10.13:2379 |   true | 58.736689ms |       |
+--------------------------+--------+-------------+-------+

/usr/local/bin/etcdctl endpoint status --write-out=table --endpoints=https://10.10.10.11:2379,https://10.10.10.12:2379,https://10.10.10.13:2379 --cacert /etc/etcd/ssl/etcd-root-ca.pem --cert /etc/etcd/ssl/etcd01.pem --key /etc/etcd/ssl/etcd01-key.pem
+--------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
|        ENDPOINT          |        ID        | VERSION | DB SIZE | IS LEADER | IS LEARNER | RAFT TERM | RAFT INDEX | RAFT APPLIED INDEX | ERRORS |
+--------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
| https://10.10.10.11:2379 | 6ed45862bf8c4ba4 |   3.5.4 |   20 kB |     false |      false |         2 |    2300514 |            2300514 |        |
| https://10.10.10.12:2379 | ae8b87e4bc780d43 |   3.5.4 |   20 kB |     false |      false |         2 |    2300514 |            2300514 |        |
| https://10.10.10.13:2379 | 50d028ea1a5a2d7f |   3.5.4 |   20 kB |      true |      false |         2 |    2300514 |            2300514 |        |
+--------------------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
```

### Backup & Restore

#### backup

Our kubernetes cluster has 1 control-plane and 3 worker nodes, as mentioned before etcd is a static pod running on control-plane, to backup our cluster we must first connect on our control-plane verify that `etcdctl` command is available (install it if not) and take a snapshot of our database.
To take a snapshot `ETCDCTL_API=3 etcdctl snapshot save /path/to/snapshot.db`

```bash
root@training-control-plane:~# etcdctl --endpoints=127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key  snapshot save /root/myetcd.db
{"level":"info","ts":"2022-07-21T09:09:19.601Z","caller":"snapshot/v3_snapshot.go:65","msg":"created temporary db file","path":"/root/myetcd.db.part"}
{"level":"info","ts":"2022-07-21T09:09:19.735Z","logger":"client","caller":"v3/maintenance.go:211","msg":"opened snapshot stream; downloading"}
{"level":"info","ts":"2022-07-21T09:09:19.735Z","caller":"snapshot/v3_snapshot.go:73","msg":"fetching snapshot","endpoint":"127.0.0.1:2379"}
{"level":"info","ts":"2022-07-21T09:09:20.179Z","logger":"client","caller":"v3/maintenance.go:219","msg":"completed snapshot read; closing"}
{"level":"info","ts":"2022-07-21T09:09:20.296Z","caller":"snapshot/v3_snapshot.go:88","msg":"fetched snapshot","endpoint":"127.0.0.1:2379","size":"3.3 MB","took":"now"}
{"level":"info","ts":"2022-07-21T09:09:20.296Z","caller":"snapshot/v3_snapshot.go:97","msg":"saved","path":"/root/myetcd.db"}
Snapshot saved at /root/myetcd.db
```
To check our snapshot status `etcdctl snapshot status /path/to/snapshot.db` here we don't have to specify endpoints and certificates as we won't contact the cluster we will just check the saved file status 

`etcdctl snapshot status` has been deprecated we should use `etcdutl` instead

```bash
root@training-control-plane:~# etcdctl --write-out=table snapshot status /root/myetcd.db
Deprecated: Use `etcdutl snapshot status` instead.

+----------+----------+------------+------------+
|   HASH   | REVISION | TOTAL KEYS | TOTAL SIZE |
+----------+----------+------------+------------+
| 8085ebc8 |     9291 |       1155 |     3.3 MB |
+----------+----------+------------+------------+

root@training-control-plane:~# etcdutl --write-out=table snapshot status myetcd.db 
+----------+----------+------------+------------+
|   HASH   | REVISION | TOTAL KEYS | TOTAL SIZE |
+----------+----------+------------+------------+
| 8085ebc8 |     9291 |       1155 |     3.3 MB |
+----------+----------+------------+------------+
```

#### Restore

To restore etcd database we use the command `etcdctl snapshot restore /path/to/mysnapshot.db` we can also choose destination directory with `--data-dir=` option to not overrwirte previous configuration if needed later. We have just to specify the new path in the etcd yaml configuration file and the pod will restart automatically as static pods restart or get recreated if the configuration file is modified. Again here we don't need to specify endpoints because we won't contact ETCD

```bash
root@training-control-plane:/# export ETCDCTL_API=3
root@training-control-plane:/# etcdctl --data-dir=/var/lib/etcd-from-backup2 snapshot restore /root/etcd-backup-20220721.db 
Deprecated: Use `etcdutl snapshot restore` instead.

2022-07-21T11:46:30+01:00	info	snapshot/v3_snapshot.go:248	restoring snapshot	{"path": "/root/etcd-backup-20220721.db", "wal-dir": "/var/lib/etcd-from-backup2/member/wal", "data-dir": "/var/lib/etcd-from-backup2", "snap-dir": "/var/lib/etcd-from-backup2/member/snap", "stack": "go.etcd.io/etcd/etcdutl/v3/snapshot.(*v3Manager).Restore\n\t/go/src/go.etcd.io/etcd/release/etcd/etcdutl/snapshot/v3_snapshot.go:254\ngo.etcd.io/etcd/etcdutl/v3/etcdutl.SnapshotRestoreCommandFunc\n\t/go/src/go.etcd.io/etcd/release/etcd/etcdutl/etcdutl/snapshot_command.go:147\ngo.etcd.io/etcd/etcdctl/v3/ctlv3/command.snapshotRestoreCommandFunc\n\t/go/src/go.etcd.io/etcd/release/etcd/etcdctl/ctlv3/command/snapshot_command.go:129\ngithub.com/spf13/cobra.(*Command).execute\n\t/go/pkg/mod/github.com/spf13/cobra@v1.1.3/command.go:856\ngithub.com/spf13/cobra.(*Command).ExecuteC\n\t/go/pkg/mod/github.com/spf13/cobra@v1.1.3/command.go:960\ngithub.com/spf13/cobra.(*Command).Execute\n\t/go/pkg/mod/github.com/spf13/cobra@v1.1.3/command.go:897\ngo.etcd.io/etcd/etcdctl/v3/ctlv3.Start\n\t/go/src/go.etcd.io/etcd/release/etcd/etcdctl/ctlv3/ctl.go:107\ngo.etcd.io/etcd/etcdctl/v3/ctlv3.MustStart\n\t/go/src/go.etcd.io/etcd/release/etcd/etcdctl/ctlv3/ctl.go:111\nmain.main\n\t/go/src/go.etcd.io/etcd/release/etcd/etcdctl/main.go:59\nruntime.main\n\t/go/gos/go1.16.15/src/runtime/proc.go:225"}
2022-07-21T11:46:30+01:00	info	membership/store.go:141	Trimming membership information from the backend...
2022-07-21T11:46:30+01:00	info	membership/cluster.go:421	added member	{"cluster-id": "cdf818194e3a8c32", "local-member-id": "0", "added-peer-id": "8e9e05c52164694d", "added-peer-peer-urls": ["http://localhost:2380"]}
2022-07-21T11:46:30+01:00	info	snapshot/v3_snapshot.go:269	restored snapshot	{"path": "/root/etcd-backup-20220721.db", "wal-dir": "/var/lib/etcd-from-backup2/member/wal", "data-dir": "/var/lib/etcd-from-backup2", "snap-dir": "/var/lib/etcd-from-backup2/member/snap"}
```
In `/etc/kubernetes/manifests/etcd.yaml` we modify `path:` under `volumes:`
```yaml
  - hostPath:
      path: /var/lib/etcd-from-backup2
      type: DirectoryOrCreate
    name: etcd-data
```

Once saved we could loose connection to the cluster for few minutes as restarting etcd will restart other pods like `kube-control-manager` and `kube-scheduler` we can check if pods restarted succesfully by looking at the lifetime of the pods should be few seconds for these pods (in this example)

```bash
root@training-control-plane:/# kubectl get pods -n kube-system
NAME                                             READY   STATUS    RESTARTS      AGE
coredns-6d4b75cb6d-mtdd4                         1/1     Running   0             3h2m
coredns-6d4b75cb6d-rwjhv                         1/1     Running   0             3h2m
etcd-training-control-plane                      1/1     Running   0             60s
kindnet-7bsbm                                    1/1     Running   0             3h2m
kindnet-bwtbd                                    1/1     Running   0             3h1m
kindnet-ncth2                                    1/1     Running   0             3h1m
kindnet-wz5d5                                    1/1     Running   0             3h1m
kube-apiserver-training-control-plane            1/1     Running   0             3h2m
kube-controller-manager-training-control-plane   1/1     Running   1 (60s ago)   3h2m
kube-proxy-c8nmk                                 1/1     Running   0             3h2m
kube-proxy-gf5dw                                 1/1     Running   0             3h1m
kube-proxy-ltzmm                                 1/1     Running   0             3h1m
kube-proxy-p9pwc                                 1/1     Running   0             3h1m
kube-scheduler-training-control-plane            1/1     Running   1 (60s ago)   3h2m
```

When kube-api is deployed as a service before restoring etcd kube-api should be stopped first

## Updating k8s cluster

The Kubernetes project maintains release branches for the most recent three minor releases (1.24, 1.23, 1.22). Kubernetes 1.19 and newer receive approximately 1 year of patch support. Kubernetes 1.18 and older received approximately 9 months of patch support.

Kubernetes versions are expressed as x.y.z, where x is the major version, y is the minor version, and z is the patch version, following Semantic Versioning terminology.

The way that you upgrade a cluster depends on how you initially deployed it and on any subsequent changes.

At a high level, the steps you perform are:

- Upgrade the control plane
- Upgrade the nodes in your cluster
- Upgrade clients such as kubectl
Adjust manifests and other resources based on the API changes that accompany the new Kubernetes version

### Before upgrade

Kubernetes can't be upgraded directly to last version from any previous one, it must always follows from minor version to next minor version for example we can't upgrade kubernetes 1.20 to 1.24 we have first to upgrade to 1.21 then from 1.21 to 1.22 and so on until the last version, the patch version doesn't matter here we can upgrade from 1.23.0 to 1.24.3 for example.

As we deployed our cluster with `kubeadm` we will use this tool to upgrade our cluster. kubeadm will upgrade all components except `etcd` (etcd can be installed on a separate cluster not on the controlplane) and `kubelet` (kubelt is not a pod but a linux process running on each node)

First of all we must update kubeadm to the new version we will deploy, we will assume that we are upgrading our cluster form 1.19 to 1.20 so the first stp is to upgrade kubeadm to kubeadm-1.20

We also try during the upgrade to minimize downtime and to not impact users and applications for that we upgrade node by node starting with the `controlplane` 

### Upgrading steps

#### Determine which version to upgrade to 

It depends if we installed kubeadm from distro repositories or from binary 
- <b>From binary</b>
  
```bash
  RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
ARCH="amd64"
cd $DOWNLOAD_DIR
sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/${ARCH}/kubeadm
sudo chmod +x kubeadm
sudo cp kubeadm /usr/local/bin
```

- <b>From Debian like distro</b>

```bash
apt update
apt-cache madison kubeadm
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.20.0-00 && \
apt-mark hold kubeadm
```

- <b>From Redhat like distro</b>

```bash
yum list --showduplicates kubeadm --disableexcludes=kubernetes
yum install -y kubeadm-1.20.0-0 --disableexcludes=kubernetes
```
We verify `kubeadm` version

```bash
root@training-control-plane:/# kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.0", GitCommit:"4ce5a8954017644c5420bae81d72b09b735c21f0", GitTreeState:"clean", BuildDate:"2022-05-19T15:39:43Z", GoVersion:"go1.18.1", Compiler:"gc", Platform:"linux/amd64"}
```

#### Upgrade controlplane 

Before upgrading the `controlplane` we will first evict all runnin pods and schedule them on other nodes for that we will `drain` the control plane node

```bash
root@controlplane:~# kubectl drain controlplane --ignore-daemonsets 
node/controlplane already cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/kube-flannel-ds-569kz, kube-system/kube-proxy-5vb6x
evicting pod default/blue-746c87566d-9kvfs
evicting pod default/blue-746c87566d-qsd5v
evicting pod kube-system/coredns-f9fd979d6-24hht
evicting pod kube-system/coredns-f9fd979d6-fmnbv
pod/blue-746c87566d-qsd5v evicted
pod/blue-746c87566d-9kvfs evicted
pod/coredns-f9fd979d6-24hht evicted
pod/coredns-f9fd979d6-fmnbv evicted
node/controlplane evicted
```

```bash
root@controlplane:~# kubectl get nodes
NAME           STATUS                     ROLES    AGE   VERSION
controlplane   Ready,SchedulingDisabled   master   45m   v1.19.0
node01         Ready                      <none>   45m   v1.19.0
```
Now the controlplane is drained and no pod will be scheduled we first upgrade kubeadm

```bash
root@controlplane:~# apt-cache madison kubeadm 
   kubeadm | 1.20.15-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.20.14-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.20.13-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.20.12-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.20.11-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.20.10-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.9-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.8-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.7-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.6-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.5-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.4-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.2-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.1-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm |  1.20.0-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.16-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.15-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.14-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.13-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.12-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.11-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
   kubeadm | 1.19.10-00 | http://apt.kubernetes.io kubernetes-xenial/main amd64 Packages
```

#### upgrade kubeadm

```bash 
root@controlplane:~# apt install kubeadm=1.20.0-00
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following held packages will be changed:
  kubeadm
The following packages will be upgraded:
  kubeadm
1 upgraded, 0 newly installed, 0 to remove and 76 not upgraded.
1 not fully installed or removed.
Need to get 7707 kB of archives.
After this operation, 111 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 kubeadm amd64 1.20.0-00 [7707 kB]
Fetched 7707 kB in 0s (28.1 MB/s)
debconf: delaying package configuration, since apt-utils is not installed
(Reading database ... 15149 files and directories currently installed.)
Preparing to unpack .../kubeadm_1.20.0-00_amd64.deb ...
Unpacking kubeadm (1.20.0-00) over (1.19.0-00) ...
Setting up kubeadm (1.20.0-00) ...
Setting up locales (2.27-3ubuntu1.6) ...
debconf: unable to initialize frontend: Dialog
debconf: (No usable dialog-like program is installed, so the dialog based frontend cannot be used. at /usr/share/perl5/Debconf/FrontEnd/Dialog.pm line 76.)
debconf: falling back to frontend: Readline
Generating locales (this might take a while)...
Generation complete.
Processing triggers for libc-bin (2.27-3ubuntu1.4) ...
Processing triggers for mime-support (3.60ubuntu1) ...
root@controlplane:~#
root@controlplane:~#
root@controlplane:~#
root@controlplane:~# kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.0", GitCommit:"af46c47ce925f4c4ad5cc8d1fca46c7b77d13b38", GitTreeState:"clean", BuildDate:"2020-12-08T17:57:36Z", GoVersion:"go1.15.5", Compiler:"gc", Platform:"linux/amd64"}
```

#### Upgrade Kubernetes components

```bash
root@controlplane:~# kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.19.0
[upgrade/versions] kubeadm version: v1.20.0
I0805 11:43:03.671926    6975 version.go:251] remote version is much newer: v1.24.3; falling back to: stable-1.20
[upgrade/versions] Latest stable version: v1.20.15
[upgrade/versions] Latest stable version: v1.20.15
[upgrade/versions] Latest version in the v1.19 series: v1.19.16
[upgrade/versions] Latest version in the v1.19 series: v1.19.16

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       AVAILABLE
kubelet     2 x v1.19.0   v1.19.16

Upgrade to the latest version in the v1.19 series:

COMPONENT                 CURRENT   AVAILABLE
kube-apiserver            v1.19.0   v1.19.16
kube-controller-manager   v1.19.0   v1.19.16
kube-scheduler            v1.19.0   v1.19.16
kube-proxy                v1.19.0   v1.19.16
CoreDNS                   1.7.0     1.7.0
etcd                      3.4.9-1   3.4.9-1

You can now apply the upgrade by executing the following command:

        kubeadm upgrade apply v1.19.16

_____________________________________________________________________

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       AVAILABLE
kubelet     2 x v1.19.0   v1.20.15

Upgrade to the latest stable version:

COMPONENT                 CURRENT   AVAILABLE
kube-apiserver            v1.19.0   v1.20.15
kube-controller-manager   v1.19.0   v1.20.15
kube-scheduler            v1.19.0   v1.20.15
kube-proxy                v1.19.0   v1.20.15
CoreDNS                   1.7.0     1.7.0
etcd                      3.4.9-1   3.4.13-0

You can now apply the upgrade by executing the following command:

        kubeadm upgrade apply v1.20.15

Note: Before you can perform this upgrade, you have to update kubeadm to v1.20.15.

_____________________________________________________________________


The table below shows the current state of component configs as understood by this version of kubeadm.
Configs that have a "yes" mark in the "MANUAL UPGRADE REQUIRED" column require manual config upgrade or
resetting to kubeadm defaults before a successful upgrade can be performed. The version to manually
upgrade to is denoted in the "PREFERRED VERSION" column.

API GROUP                 CURRENT VERSION   PREFERRED VERSION   MANUAL UPGRADE REQUIRED
kubeproxy.config.k8s.io   v1alpha1          v1alpha1            no
kubelet.config.k8s.io     v1beta1           v1beta1             no
_____________________________________________________________________


root@controlplane:~# kubeadm upgrade apply v1.20.0
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade/version] You have chosen to change the cluster version to "v1.20.0"
[upgrade/versions] Cluster version: v1.19.0
[upgrade/versions] kubeadm version: v1.20.0
[upgrade/confirm] Are you sure you want to proceed with the upgrade? [y/N]: y
[upgrade/prepull] Pulling images required for setting up a Kubernetes cluster
[upgrade/prepull] This might take a minute or two, depending on the speed of your internet connection
[upgrade/prepull] You can also perform this action in beforehand using 'kubeadm config images pull'
[upgrade/apply] Upgrading your Static Pod-hosted control plane to version "v1.20.0"...
Static pod: kube-apiserver-controlplane hash: dc1ece12c7bd04024ee06adf6114f4d3
Static pod: kube-controller-manager-controlplane hash: f6a9bf2865b2fe580f39f07ed872106b
Static pod: kube-scheduler-controlplane hash: 5146743ebb284c11f03dc85146799d8b
[upgrade/etcd] Upgrading to TLS for etcd
Static pod: etcd-controlplane hash: a5c546b5eef79dad7357de271b3ab50c
[upgrade/staticpods] Preparing for "etcd" upgrade
[upgrade/staticpods] Renewing etcd-server certificate
[upgrade/staticpods] Renewing etcd-peer certificate
[upgrade/staticpods] Renewing etcd-healthcheck-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/etcd.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2022-08-05-11-46-19/etcd.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: etcd-controlplane hash: a5c546b5eef79dad7357de271b3ab50c
Static pod: etcd-controlplane hash: a5c546b5eef79dad7357de271b3ab50c
Static pod: etcd-controlplane hash: 339a1b3e24fc55eafc872f833c501b87
[apiclient] Found 1 Pods for label selector component=etcd
[upgrade/staticpods] Component "etcd" upgraded successfully!
[upgrade/etcd] Waiting for etcd to become available
[upgrade/staticpods] Writing new Static Pod manifests to "/etc/kubernetes/tmp/kubeadm-upgraded-manifests149466491"
[upgrade/staticpods] Preparing for "kube-apiserver" upgrade
[upgrade/staticpods] Renewing apiserver certificate
[upgrade/staticpods] Renewing apiserver-kubelet-client certificate
[upgrade/staticpods] Renewing front-proxy-client certificate
[upgrade/staticpods] Renewing apiserver-etcd-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-apiserver.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2022-08-05-11-46-19/kube-apiserver.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-apiserver-controlplane hash: dc1ece12c7bd04024ee06adf6114f4d3
Static pod: kube-apiserver-controlplane hash: dc1ece12c7bd04024ee06adf6114f4d3
Static pod: kube-apiserver-controlplane hash: dc1ece12c7bd04024ee06adf6114f4d3
Static pod: kube-apiserver-controlplane hash: dc1ece12c7bd04024ee06adf6114f4d3
Static pod: kube-apiserver-controlplane hash: f319cb8dbeea7d48b8e6fda1c386bf33
[apiclient] Found 1 Pods for label selector component=kube-apiserver
[upgrade/staticpods] Component "kube-apiserver" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-controller-manager" upgrade
[upgrade/staticpods] Renewing controller-manager.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-controller-manager.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2022-08-05-11-46-19/kube-controller-manager.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-controller-manager-controlplane hash: f6a9bf2865b2fe580f39f07ed872106b
Static pod: kube-controller-manager-controlplane hash: f6a9bf2865b2fe580f39f07ed872106b
Static pod: kube-controller-manager-controlplane hash: a875134e700993a22f67999011829566
[apiclient] Found 1 Pods for label selector component=kube-controller-manager
[upgrade/staticpods] Component "kube-controller-manager" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-scheduler" upgrade
[upgrade/staticpods] Renewing scheduler.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-scheduler.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2022-08-05-11-46-19/kube-scheduler.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-scheduler-controlplane hash: 5146743ebb284c11f03dc85146799d8b
Static pod: kube-scheduler-controlplane hash: 81d2d21449d64d5e6d5e9069a7ca99ed
[apiclient] Found 1 Pods for label selector component=kube-scheduler
[upgrade/staticpods] Component "kube-scheduler" upgraded successfully!
[upgrade/postupgrade] Applying label node-role.kubernetes.io/control-plane='' to Nodes with label node-role.kubernetes.io/master='' (deprecated)
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.20" in namespace kube-system with the configuration for the kubelets in the cluster
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.20.0". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.
```

We verify control plane version 

```bash
root@controlplane:~# kubectl get nodes
NAME           STATUS                     ROLES                  AGE   VERSION
controlplane   Ready,SchedulingDisabled   control-plane,master   76m   v1.19.0
node01         Ready                      <none>                 75m   v1.19.0
```

We notice that it shows version 1.19.0 even if the upgrade succeded. this is because kubelet is still in version 1.19.0 and must be upgraded separately

#### Upgrade Kubelet

As mentioned before `kubelet` is a linux process as for `kubeadm` we perform upgrade from package manager
```bash
root@controlplane:~# apt install kubelet=1.20.0-00   
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following held packages will be changed:
  kubelet
The following packages will be upgraded:
  kubelet
1 upgraded, 0 newly installed, 0 to remove and 76 not upgraded.
Need to get 18.8 MB of archives.
After this operation, 4000 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 kubelet amd64 1.20.0-00 [18.8 MB]
Fetched 18.8 MB in 0s (49.3 MB/s)
debconf: delaying package configuration, since apt-utils is not installed
(Reading database ... 15149 files and directories currently installed.)
Preparing to unpack .../kubelet_1.20.0-00_amd64.deb ...
/usr/sbin/policy-rc.d returned 101, not running 'stop kubelet.service'
Unpacking kubelet (1.20.0-00) over (1.19.0-00) ...
Setting up kubelet (1.20.0-00) ...
/usr/sbin/policy-rc.d returned 101, not running 'start kubelet.service'
```

- Restart service

`root@controlplane:~# sudo systemctl restart kubelet.service `

- Check service status (ensure that it's running)

```bash
root@controlplane:~# sudo systemctl status kubelet.service 
● kubelet.service - kubelet: The Kubernetes Node Agent
   Loaded: loaded (/lib/systemd/system/kubelet.service; enabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/kubelet.service.d
           └─10-kubeadm.conf
   Active: active (running) since Fri 2022-08-05 11:53:52 UTC; 6s ago
     Docs: https://kubernetes.io/docs/home/
 Main PID: 16756 (kubelet)
    Tasks: 29 (limit: 5529)
   CGroup: /system.slice/kubelet.service
           └─16756 /usr/bin/kubelet --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf -

Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.250903   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.250920   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.250974   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251013   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251066   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251089   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251111   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251133   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251179   16756 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolum
Aug 05 11:53:56 controlplane kubelet[16756]: I0805 11:53:56.251295   16756 reconciler.go:157] Reconciler: start to sync state
```

- Check control plane version 

```bash
root@controlplane:~# kubectl get nodes
NAME           STATUS                     ROLES                  AGE   VERSION
controlplane   Ready,SchedulingDisabled   control-plane,master   81m   v1.20.0
node01         Ready                      <none>                 80m   v1.19.0
root@controlplane:~# 
```

#### Upgrade kubectl

We upgrade kubectl and that's all `apt install kubectl=1.20.0-00`

```bash
kubectl version --short
Client Version: v1.20.0
Server Version: v1.20.0
```

Notice that we have to do all these steps in each control plane node we have in our cluster

Finally we uncordon the control plane node

```bash
kubectl uncordon controlplane 
node/controlplane uncordoned
```

```bash
kubectl get nodes
NAME           STATUS   ROLES                  AGE   VERSION
controlplane   Ready    control-plane,master   89m   v1.20.0
node01         Ready    <none>                 88m   v1.19.0
```

Control plane is now ready

##### Upgrade Worker nodes

Worker node upgrade is much quicker than control plane first we `drain` worker node to schedule running pods on another worker node here we have only a control plane we can get rid off the `taint` to allow schduling pods on control plane during the node upgrade but in production we should have at least 3 worker nodes

```bash
kubectl drain node01 --ignore-daemonsets
node/node01 already cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/kube-flannel-ds-w2gq9, kube-system/kube-proxy-qtsc8
evicting pod kube-system/coredns-74ff55c5b-96lzb
evicting pod default/blue-746c87566d-pxqqh
evicting pod default/blue-746c87566d-rp98w
evicting pod default/blue-746c87566d-cl87m
evicting pod default/blue-746c87566d-g5mns
evicting pod kube-system/coredns-74ff55c5b-64ctm
evicting pod default/blue-746c87566d-p55zs
I0805 12:18:59.283431   21863 request.go:655] Throttling request took 1.094801479s, request: GET:https://controlplane:6443/api/v1/namespaces/default/pods/blue-746c87566d-pxqqh
pod/blue-746c87566d-p55zs evicted
pod/coredns-74ff55c5b-96lzb evicted
pod/coredns-74ff55c5b-64ctm evicted
pod/blue-746c87566d-g5mns evicted
pod/blue-746c87566d-cl87m evicted
pod/blue-746c87566d-pxqqh evicted
pod/blue-746c87566d-rp98w evicted
node/node01 evicted
```

```bash
kubectl get nodes
NAME           STATUS                     ROLES                  AGE   VERSION
controlplane   Ready                      control-plane,master   14m   v1.20.0
node01         Ready,SchedulingDisabled   <none>                 14m   v1.19.0
```

As before we upgrade kubeadm, kubectl and kublet and then launch command `kubeadm upgrade node` on worker node

```bash
kubeadm upgrade node        
[upgrade] Reading configuration from the cluster...
[upgrade] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks
[preflight] Skipping prepull. Not a control plane node.
[upgrade] Skipping phase. Not a control plane node.
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[upgrade] The configuration for this node was successfully updated!
[upgrade] Now you should go ahead and upgrade the kubelet package using your package manager.

systemctl restart kubelet

systemct status kubelet
● kubelet.service - kubelet: The Kubernetes Node Agent
   Loaded: loaded (/lib/systemd/system/kubelet.service; enabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/kubelet.service.d
           └─10-kubeadm.conf
   Active: active (running) since Fri 2022-08-05 12:26:56 UTC; 3s ago
     Docs: https://kubernetes.io/docs/home/
 Main PID: 23820 (kubelet)
    Tasks: 26 (limit: 5529)
   CGroup: /system.slice/kubelet.service
           └─23820 /usr/bin/kubelet --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf -

Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.229320   23820 topology_manager.go:187] [topologymanager] Topology Admit Handler
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324072   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324106   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324123   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324140   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324161   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324182   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324288   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324334   23820 reconciler.go:224] operationExecutor.VerifyControllerAttachedVolume star
Aug 05 12:26:58 node01 kubelet[23820]: I0805 12:26:58.324346   23820 reconciler.go:157] Reconciler: start to sync state
```

```bash
kubectl get nodes
NAME           STATUS                     ROLES                  AGE   VERSION
controlplane   Ready                      control-plane,master   20m   v1.20.0
node01         Ready,SchedulingDisabled   <none>                 20m   v1.20.0
```

```bash
kubectl uncordon node01 
node/node01 uncordoned
```


## Useful imperative commands

#### Create/start a pod with nginx image
```bash
kubectl run pod_name --image=nginx 
```

#### Create/start a pod with nginx image and epose port 80
```bash
kubectl run pod_name --image=nginx --port=80
```
#### Create/start mariadb with environment variables "MARIADB_USER=myuser" and "MARIADB_DATABASE=MyDB" in the container
```bash
kubectl run mariadb --image=mariadb:latest --env="MARIADB_USER=myuser" --env="MARIADB_DATABASE=MyDB"
```
#### Create/Start nginx pod and set labels "app=nginx" and "env=prod"
```bash
kubectl run nginx --image=nginx --labels="app=hazelcast,env=prod"
```

#### Create/start a pod and create a service at once
```bash
kubectl run pod_name --image=nginx --port=80 --expose
```
